// const path = require('path');
// const { CleanWebpackPlugin } = require("clean-webpack-plugin");
// // const BundleAnalyzerPlugin = require('webpack-bundle-analyzer').BundleAnalyzerPlugin

// module.exports = {
//   entry: {
//     index: './src/index.js',
//   },
//   output: {
//     globalObject: 'this',
//     path: path.resolve(__dirname, 'dist'),
//     filename: '[name].bundle.js',
//     library: 'alpha-ui',
//     libraryTarget: 'umd',
//   },
//   externals: {
//     react: 'react',
//     'react-dom': 'react-dom',
//   },
//   // resolve: {
//   //   extensions: ['.js'],
//   // },
//   module: {
//     rules: [
//       {
//         test: /\.css$/i,
//         exclude: /tailwind/,
//         use: ['style-loader', 'css-loader'],
//       },
//       {
//         test: /\.js?$/,
//         loader: 'babel-loader',
//         exclude: /node_modules/,
//       },
//     ],
//   },
//   plugins: [new CleanWebpackPlugin()],
//   // plugins: [new BundleAnalyzerPlugin()],
// };
