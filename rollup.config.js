// import multiInput from 'rollup-plugin-multi-input';
import peerDepsExternal from "rollup-plugin-peer-deps-external";
import babel from "rollup-plugin-babel";
import resolve from "@rollup/plugin-node-resolve";
import commonjs from "@rollup/plugin-commonjs";
// import typescript from "rollup-plugin-typescript2";
import postcss from "rollup-plugin-postcss";
import copy from "rollup-plugin-copy";

const packageJson = require("./package.json");

export default [
  {
    input: "src/index.js",
    output: [
      {
        file: packageJson.main,
        format: "cjs",
        sourcemap: true
      },
      {
        file: packageJson.module,
        format: "esm",
        sourcemap: true
      }
    ],
    plugins: [
      peerDepsExternal(),
      babel({
        exclude: 'node_modules/**',
        //      plugins: [ '@babel/plugin-external-helpers' ]
      }),
      resolve(),
      commonjs(),
      // typescript({ useTsconfigDeclarationDir: true }),
      postcss(),
      // copy({
      //   targets: [
      //     {
      //       src: "./config.js",
      //       dest: "dist",
      //       rename: "config.js"
      //     },
      //     // {
      //     //   src: "src/variables.scss",
      //     //   dest: "build",
      //     //   rename: "variables.scss"
      //     // },
      //     // {
      //     //   src: "src/typography.scss",
      //     //   dest: "build",
      //     //   rename: "typography.scss"
      //     // }
      //   ]
      // })
    ]
  },
  {
    input: "config.js",
    output: [
      {
        file: 'dist/config.js',
        format: "cjs",
        sourcemap: true
      },
      {
        file: 'dist/config.esm.js',
        format: "esm",
        sourcemap: true
      }
    ],
    plugins: [
      peerDepsExternal(),
      babel({
        exclude: 'node_modules/**',
        //      plugins: [ '@babel/plugin-external-helpers' ]
      }),
      resolve(),
      commonjs(),
      // typescript({ useTsconfigDeclarationDir: true }),
      postcss(),
    ]
  }
];
