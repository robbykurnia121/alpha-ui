export {
  MdAdd as AddIcon,
  MdRemove as RemoveIcon,
  // ArrowBackIos as BackIcon,
  MdMoreHoriz as MoreIcon,
  MdSort as SortIcon,
  MdArrowBack as PrevIcon,
  MdArrowForward as NextIcon,
  MdExpandMore as ExpandIcon,
  MdArrowDropDown as DropDownIcon,
  MdClose as CloseIcon,
  MdClear as ClearIcon,
  MdSend as SendIcon,
  MdDelete as DeleteIcon,
  MdCancel as CancelIcon,
  MdSave as SaveIcon,
  MdDone as DoneIcon,
  MdPhotoLibrary as PhotoLibraryIcon,
  MdEmail as EmailIcon,
  MdComment as CommentIcon,
  MdShare as ShareIcon,
  MdCheckBox as checkBoxIcon,
  MdCheckBoxOutlineBlank as uncheckBoxIcon,
} from 'react-icons/md';

export {
  BiSearch as SearchIcon,
  BiCalendarEvent as CalendarIcon
} from 'react-icons/bi';
export {
  FiEdit as EditIcon,
} from 'react-icons/fi';

export {
  BsPersonFill as PersonIcon,
} from 'react-icons/bs';

export {
  ImHome as HomeIcon,
  ImHistory as HistoryIcon,
  ImUser as AccountIcon,
  ImBubbles as ChatIcon,
  ImDrawer as InboxIcon,
} from 'react-icons/im';