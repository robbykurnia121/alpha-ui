import React, { useMemo } from 'react';

// export interface State {
//   displaySidebar: boolean
//   displayDropdown: boolean
//   displayModal: boolean
//   displayToast: boolean
//   modalView: string
//   toastText: string
// }

const initialState = {
  displaySidebar: false,
  displayDropdown: false,
  displayModal: false,
  modalView: 'LOGIN_VIEW',
  modalViewProps: null,
  displayToast: false,
  toastText: '',
  transparentNavbar: false,
  overlayNavbar: false,
};

// type Action =
//   | {
//       type: 'OPEN_SIDEBAR'
//     }
//   | {
//       type: 'CLOSE_SIDEBAR'
//     }
//   | {
//       type: 'OPEN_TOAST'
//     }
//   | {
//       type: 'CLOSE_TOAST'
//     }
//   | {
//       type: 'SET_TOAST_TEXT'
//       text: ToastText
//     }
//   | {
//       type: 'OPEN_DROPDOWN'
//     }
//   | {
//       type: 'CLOSE_DROPDOWN'
//     }
//   | {
//       type: 'OPEN_MODAL'
//     }
//   | {
//       type: 'CLOSE_MODAL'
//     }
//   | {
//       type: 'SET_MODAL_VIEW'
//       view: MODAL_VIEWS
//     }

// type MODAL_VIEWS = 'SIGNUP_VIEW' | 'LOGIN_VIEW' | 'FORGOT_VIEW'
// type ToastText = string

export const UIContext = React.createContext(initialState);

UIContext.displayName = 'UIContext';

function uiReducer(state, action) {
  switch (action.type) {
    case 'OPEN_SIDEBAR': {
      return {
        ...state,
        displaySidebar: true,
      };
    }
    case 'CLOSE_SIDEBAR': {
      return {
        ...state,
        displaySidebar: false,
      };
    }
    case 'OPEN_DROPDOWN': {
      return {
        ...state,
        displayDropdown: true,
      };
    }
    case 'CLOSE_DROPDOWN': {
      return {
        ...state,
        displayDropdown: false,
      };
    }
    case 'OPEN_MODAL': {
      return {
        ...state,
        displayModal: true,
        modalView: action.view,
        modalViewProps: action.viewProps,
      };
    }
    case 'CLOSE_MODAL': {
      return {
        ...state,
        displayModal: false,
      };
    }
    case 'OPEN_TOAST': {
      return {
        ...state,
        displayToast: true,
      };
    }
    case 'CLOSE_TOAST': {
      return {
        ...state,
        displayToast: false,
      };
    }
    case 'SET_MODAL_VIEW': {
      return {
        ...state,
        modalView: action.view,
        modalViewProps: action.viewProps,
      };
    }
    case 'SET_TOAST_TEXT': {
      return {
        ...state,
        toastText: action.text,
      };
    }
    case 'SET_TRANSPARENT_NAVBAR': {
      return {
        ...state,
        transparentNavbar: Boolean(action.transparent),
      };
    }
    case 'SET_OVERLAY_NAVBAR': {
      return {
        ...state,
        overlayNavbar: Boolean(action.overlay),
      };
    }
    default:
      return { ...state };
  }
}

export const UIProvider = (props) => {
  const [state, dispatch] = React.useReducer(uiReducer, initialState);
  // const 
  const openSidebar = () => dispatch({ type: 'OPEN_SIDEBAR' });
  const closeSidebar = () => dispatch({ type: 'CLOSE_SIDEBAR' });
  const toggleSidebar = () =>
    state.displaySidebar
      ? dispatch({ type: 'CLOSE_SIDEBAR' })
      : dispatch({ type: 'OPEN_SIDEBAR' });
  const closeSidebarIfPresent = () =>
    state.displaySidebar && dispatch({ type: 'CLOSE_SIDEBAR' });

  const openDropdown = () => dispatch({ type: 'OPEN_DROPDOWN' });
  const closeDropdown = () => dispatch({ type: 'CLOSE_DROPDOWN' });

  const openModal = (view, viewProps) => dispatch({ type: 'OPEN_MODAL', view, viewProps });
  const closeModal = () => dispatch({ type: 'CLOSE_MODAL' });

  const openToast = () => dispatch({ type: 'OPEN_TOAST' });
  const closeToast = () => dispatch({ type: 'CLOSE_TOAST' });

  const setModalView = (view, viewProps) =>
    dispatch({ type: 'SET_MODAL_VIEW', view, viewProps });

  const setTransparentNavbar = (transparent = true) => dispatch({ type: 'SET_TRANSPARENT_NAVBAR', transparent });
  const setOverlayNavbar = (overlay) => dispatch({ type: 'SET_OVERLAY_NAVBAR', overlay });

  const value = useMemo(
    () => ({
      ...state,
      openSidebar,
      closeSidebar,
      toggleSidebar,
      closeSidebarIfPresent,
      openDropdown,
      closeDropdown,
      openModal,
      closeModal,
      setModalView,
      openToast,
      closeToast,
      setTransparentNavbar,
      setOverlayNavbar,
    }),
    [state]
  );

  return <UIContext.Provider value={value} {...props} />;
};

export const useUI = () => {
  const context = React.useContext(UIContext);
  if (context === undefined) {
    throw new Error(`useUI must be used within a UIProvider`);
  }
  return context;
};
