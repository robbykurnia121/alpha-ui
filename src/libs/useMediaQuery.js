import { useState, useEffect } from "react";

const breakpoints = {
  'xs-up': 360,
  'sm-up': 640,
  'md-up': 1024,
  'lg-up': 1280,
};

const useMediaQuery = (size = 'xs-up') => {
  const breakpoint = breakpoints[size];
  const [isMatch, setIsMatch] = useState(false);



  useEffect(() => {
    function handleResize() {
      const newIsMatch = window.matchMedia(`(min-width: ${breakpoint}px)`).matches;
      if (isMatch !== newIsMatch) {
        setIsMatch(newIsMatch);
      }
    }

    // initial size
    handleResize();

    window.addEventListener("resize", handleResize);
    return () => {
      window.removeEventListener("resize", handleResize);
    };
  }, [isMatch]);
  return isMatch;
};

export default useMediaQuery;