import React from 'react';
import Table from './Table';

export default {
  title: 'Container/Table',
  component: Table,
  argTypes: {
  },
};

const Template = (args) => <Table {...args} />;

export const Default = Template.bind({});
Default.args = {
};