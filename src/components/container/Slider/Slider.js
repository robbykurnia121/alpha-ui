
import React, { useState } from "react";
import { bool, number, oneOf, arrayOf, any, shape, oneOfType, string } from "prop-types";
import { Swiper, SwiperSlide } from "swiper/react";

// import Swiper core and required modules
import SwiperCore, {
  Pagination, Navigation, Thumbs
} from 'swiper/core';

// Import Swiper styles
import "swiper/swiper.min.css";
import "swiper/components/pagination/pagination.min.css";
import "swiper/components/navigation/navigation.min.css";
import "swiper/components/thumbs/thumbs.min.css";
// import "./swiper.css";
import { useMediaQuery } from "../../../libs";

// install Swiper modules
SwiperCore.use([Pagination, Navigation, Thumbs]);

export default function Slider({
  slidesPerView, spaceBetween, loop, paginationStyle, style, arrowStyle, shadowSize, thumbnail,
  paginationType, data, children
}) {
  const [swiper, setSwiper] = useState(null);
  const [thumbsSwiper, setThumbsSwiper] = useState(null);
  const mdUp = useMediaQuery('md-up');

  const next = () => {
    swiper.slideNext();
  };
  const prev = () => {
    swiper.slidePrev();
  };
  return (
    <div>

      <div className='flex items-center' style={style}>

        {/* Prev Arrow */}
        <div onClick={prev} className={`swiper-button-prev text-primary `}
          style={{
            top: 'unset',
            position: 'absolute',
            display: arrowStyle === 'hidden' || !mdUp ? 'none' : '',
            transform: `translate(${arrowStyle === 'outside' ? '0rem' : '4rem'},10px)`,
            // color: 'var(--text-primary)'
          }}
        />

        <Swiper
          onInit={(swiper) => {
            if (shadowSize === 'medium') {
              swiper.el.style.marginLeft = '-12px';
              swiper.el.style.marginBottom = '-12px';
              swiper.el.style.paddingLeft = '12px';
              swiper.el.style.paddingTop = '12px';
              swiper.el.style.paddingBottom = '12px';
            }
            if (shadowSize === 'large') {
              swiper.el.style.marginLeft = '-32px';
              swiper.el.style.marginBottom = '-32px';
              swiper.el.style.paddingLeft = '32px';
              swiper.el.style.paddingTop = '32px';
              swiper.el.style.paddingBottom = '32px';
            }
            swiper.navigation.init();
            swiper.navigation.update();
          }}
          onSwiper={(s) => {
            setSwiper(s);
          }}
          thumbs={{ swiper: thumbsSwiper }}
          className="mySwiper"
          slidesPerView={slidesPerView}
          spaceBetween={spaceBetween}
          pagination={{
            type: paginationType,
            clickable: true,
          }}
          loop={loop}
        >
          {data && data.map((item, index) => (
            <SwiperSlide key={index}>
              {item}
            </SwiperSlide>
          ))}
          {children && children.map((child, index) => (
            <SwiperSlide key={index}>
              {child}
            </SwiperSlide>
          ))}
        </Swiper>

        {/* Next Arrow */}
        <div onClick={next} className={`swiper-button-next text-primary `}
          style={{
            top: 'unset',
            position: 'absolute',
            display: arrowStyle === 'hidden' || !mdUp ? 'none' : '',
            transform: `translate(${arrowStyle === 'outside' ? '0rem' : '-4rem'},10px)`,
            // color: 'var(--text-primary)'
          }} />
      </div>
      {thumbnail && (
        <>
          <div className='my-2'></div>
          <Swiper
            onSwiper={setThumbsSwiper}
            spaceBetween={10}
            slidesPerView={5}
            freeMode={true}
            watchSlidesVisibility={true}
            watchSlidesProgress={true}
            className="mySwiper2">
            {data && data.map((item, index) => (
              <SwiperSlide key={index}>
                {item}
              </SwiperSlide>
            ))}
            {children && children.map((child, index) => (
              <SwiperSlide key={index}>
                {child}
              </SwiperSlide>
            ))}
          </Swiper>
        </>
      )}
      <style jsx='true'>
        {`
        .swiper-container {
          width: 100%;
          height: 100%;
        }
        
        .swiper-pagination-bullets{
          padding-left: 1rem;
          padding-right: 1rem;
          // text-align: left;
        }
        .swiper-pagination-bullet{
          background: white !important;
          opacity: .4 !important;
          width: 10px;
          height: 10px;
          border-radius: 8px;
        }
        
        .swiper-pagination-bullets:only-child {
          visibility: hidden;
        }
        
        .swiper-pagination-bullet-active{
          opacity: 1 !important;
        }
        
        .swiper-slide {
          text-align: center;
          display: -webkit-box;
          display: -ms-flexbox;
          display: -webkit-flex;
          display: flex;
          -webkit-box-pack: center;
          -ms-flex-pack: center;
          -webkit-justify-content: center;
          justify-content: center;
          -webkit-box-align: center;
          -ms-flex-align: center;
          -webkit-align-items: center;
          align-items: center;
        }
        
        .swiper-slide img {
          display: block;
          width: 100%;
          height: 100%;
        }
        
        .mySwiper2 .swiper-slide {
          filter: brightness(65%);
        }
        
        .mySwiper2 .swiper-slide-thumb-active {
          filter: brightness(100%);
        }
        `}
      </style>
    </div>
  );
}

Slider.propTypes = {
  slidesPerView: oneOfType([number, string]),
  spaceBetween: number,
  style: shape({}),
  paginationType: oneOf(['hidden', 'bullets', 'progressbar', 'fraction']),
  paginationStyle: oneOf(['hidden', 'outside', 'inside']),
  arrowStyle: oneOf(['hidden', 'outside', 'inside']),
  loop: bool,
  thumbnail: bool,
  data: arrayOf(any),
  children: any,
  shadowSize: oneOf(['none', 'medium', 'large'])
};

Slider.defaultProps = {
  slidesPerView: 1, // use auto for dynamics width
  spaceBetween: 16,
  style: {},
  paginationType: 'hidden',
  paginationStyle: 'hidden',
  arrowStyle: 'hidden',
  loop: false,
  thumbnail: false,
  data: null,
  children: null,
  shadowSize: 'none'
};;