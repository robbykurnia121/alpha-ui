import React from 'react';
import Slider from './Slider';
import { ProfileCard } from '../Card/Card.stories.js';

export default {
  title: 'Container/Slider',
  component: Slider,
  argTypes: {
  },
};

const Template = (args) => <div className='mx-12'>
  <Slider {...args} />
</div>;

export const Default = Template.bind({});
Default.args = {
  data: [
    <img alt='' src="https://swiperjs.com/demos/images/nature-1.jpg" className='rounded-large' />,
    <img alt='' src="https://swiperjs.com/demos/images/nature-2.jpg" className='rounded-large' />,
    <img alt='' src="https://swiperjs.com/demos/images/nature-3.jpg" className='rounded-large' />,
    <img alt='' src="https://swiperjs.com/demos/images/nature-4.jpg" className='rounded-large' />,
    <img alt='' src="https://swiperjs.com/demos/images/nature-5.jpg" className='rounded-large' />,
    <img alt='' src="https://swiperjs.com/demos/images/nature-6.jpg" className='rounded-large' />
  ]
};

const ProfileSliderTemplate = (args) => (
  <Slider {...args}>
    <ProfileCard {...ProfileCard.args} />
    <ProfileCard {...ProfileCard.args} />
    <ProfileCard {...ProfileCard.args} />
    <ProfileCard {...ProfileCard.args} />
    <ProfileCard {...ProfileCard.args} />
    <ProfileCard {...ProfileCard.args} />
  </Slider>
);

export const ProfileSlider = ProfileSliderTemplate.bind({});
ProfileSlider.args = {
  slidesPerView: 3
};
