import React from 'react';
import { any, oneOf, shape } from "prop-types";
import Text from "../../basic/Text/Text";

export default function CardSubtitle({ primary, secondary, primaryProps, secondaryProps, children }) {
  return (
    <div>
      {primary && (
        <Text component='div' variant='body2' {...primaryProps}>
          {primary}
        </Text>
      )}
      {secondary && (
        <Text component='div' variant='body2' {...secondaryProps}>
          {secondary}
        </Text>
      )}
      {children}
    </div>
  );
};


CardSubtitle.propTypes = {
  variant: oneOf(['profile', 'product', 'video', 'default', 'default2']),
  primary: any,
  secondary: any,
  primaryProps: shape({}),
  secondaryProps: shape({})
};

CardSubtitle.defaultProps = {
  variant: 'default',
  primary: null,
  secondary: null,
  primaryProps: {},
  secondaryProps: {}
};
