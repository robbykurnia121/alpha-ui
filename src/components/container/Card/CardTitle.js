import React from 'react';
import { any, oneOf, shape } from "prop-types";
import Text from "../../basic/Text/Text";

export default function CardTitle({ primary, secondary, primaryProps, secondaryProps, children }) {
  return (
    <div>
      {primary && (
        <Text component='div' variant='body1' {...primaryProps} gutterBottom>
          {primary}
        </Text>
      )}
      {secondary && (
        <Text component='div' variant='body2' {...secondaryProps} gutterBottom>
          {secondary}
        </Text>
      )}
      {children}
    </div>
  );
};


CardTitle.propTypes = {
  variant: oneOf(['profile', 'product', 'video', 'default', 'default2']),
  primary: any,
  secondary: any,
  primaryProps: shape({}),
  secondaryProps: shape({})
};

CardTitle.defaultProps = {
  variant: 'default',
  primary: null,
  secondary: null,
  primaryProps: {},
  secondaryProps: {}
};
