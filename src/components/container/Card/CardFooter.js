import React from 'react';
import { any, shape } from "prop-types";
import Text from "../../basic/Text/Text";

export default function CardFooter({ primary, secondary, primaryProps, secondaryProps, children }) {
  return (
    <>
      {primary?.props?.children?.length > 0 && (
        <div className='flex items-center'>
          {primary.props.children.map((child, index) => (
            <Text key={index} component='div' variant='body2' {...primaryProps}>
              {child}
            </Text>
          ))}
        </div>
      )}
      {secondary && (
        <Text component='div' variant='body2' {...secondaryProps}>
          {secondary}
        </Text>
      )}
      {children && (
        <div className='flex flex-1 items-center justify-between'>
          {children}
        </div>
      )}
    </>
  );
};


CardFooter.propTypes = {
  // variant: oneOf(['profile', 'product', 'video', 'default', 'default2']),
  primary: any,
  secondary: any,
  primaryProps: shape({}),
  secondaryProps: shape({})

};

CardFooter.defaultProps = {
  // variant: 'default',
  primary: null,
  secondary: null,
  primaryProps: {},
  secondaryProps: {}
};
