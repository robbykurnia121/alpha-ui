import React from 'react';
import { func, node, oneOf, oneOfType } from "prop-types";

export default function CardMedia({ variant, src, component, title = '', alt = '', aspectRatio = 1 }) {
  return (
    <>
      {variant === 'profile' ? (
        <div className='mr-4'>
          {component && component({ src, title, alt, aspectRatio, radius: "50%" })}
        </div>
      ) : (
        component && component({ src, title, alt, aspectRatio })
      )}
    </>
  );
};


CardMedia.propTypes = {
  variant: oneOf(['profile', 'product', 'video', 'default', 'default2']),
  primary: oneOfType([func, node]),
  secondary: oneOfType([func, node]),

};

CardMedia.defaultProps = {
  variant: 'default',
  primary: null,
  secondary: null,
};