import React, { Children, cloneElement, isValidElement } from 'react';
import CardBody from './CardBody';
import CardSubtitle from './CardSubtitle';
import CardTitle from './CardTitle';

const col1 = [CardTitle, CardSubtitle];
const col2 = [CardBody];

export default function CardContent({ children }) {

  const renderChildren = (component) => {
    return Children.map(children, e => {
      if (isValidElement(e) && component?.includes(e.type)) {
        return cloneElement(e, {});
      }
    });
  };
  return (
    <div className='w-full'>
      <div className='flex'>
        <div className='text-left'>{renderChildren(col1)}</div>
        {renderChildren(col2).length > 0 && <div className='ml-auto text-right'>{renderChildren(col2)}</div>}
      </div>
    </div>);
}
