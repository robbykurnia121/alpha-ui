import React from 'react';
import { any, shape } from "prop-types";
import Text from "../../basic/Text/Text";

export default function CardBody({ primary, secondary, primaryProps, secondaryProps, children }) {
  return (
    <>
      {primary && (
        <Text component='div' variant='body2' maxLine={3} {...primaryProps}>
          {primary}
        </Text>
      )}
      {secondary && (
        <Text component='div' variant='body2' maxLine={3} {...secondaryProps}>
          {secondary}
        </Text>
      )}
      {children}
    </>
  );
};


CardBody.propTypes = {
  // variant: oneOf(['profile', 'product', 'video', 'default', 'default2']),
  primary: any,
  secondary: any,
  primaryProps: shape({}),
  secodnaryProps: shape({})
};

CardBody.defaultProps = {
  // variant: 'default',
  primary: null,
  secondary: null,
  primaryProps: {},
  secodnaryProps: {}
};
