import React from "react";
import { any, arrayOf } from "prop-types";
import { Button } from "../../basic";

export default function CardAction({ buttons }) {
  return (<div>{buttons.map((button, index) => (
    <Button key={index} className='my-2' {...button} />
  ))}</div>);
};
CardAction.propTypes = {
  buttons: arrayOf(any),
};

CardAction.defaultProps = {
  buttons: null,
};