import React, { Children, cloneElement, isValidElement } from 'react';
import { oneOf, string } from 'prop-types';
import CardMedia from './CardMedia';
import CardContent from './CardContent';
import CardFooter from './CardFooter';
import CardAction from './CardAction';
import Box from '../../basic/Box/Box';
import Divider from '../../basic/Divider/Divider';


export default function Card({
  children, variant, backgroundImage, rotation, width, height, shadowSize
}) {
  const hasChildren = (component) => {
    return Children.map(children, e => {
      return isValidElement(e) && e.type === component;
    }).filter(e => e).length > 0;
  };

  const renderChildren = (component) => {
    return Children.map(children, e => {
      if (isValidElement(e) && e.type === component) {
        return cloneElement(e, { variant });
      }
    });
  };

  const renderFooters = () => {
    return (
      <div className='flex'>
        {renderChildren(CardFooter)}
        <div className='ml-auto'>{renderChildren(CardAction)}</div>
      </div>
    );
  };


  const renderProfile = () => {
    return (
      <Box color='transparent' shadowSize={shadowSize || 'medium'} variant='outlined' style={{ width, height }}>
        <div className='flex items-center'>
          {renderChildren(CardMedia)}
          {renderChildren(CardContent)}
        </div>
        {hasChildren(CardFooter) &&
          <>
            <div className='py-2'>
              <Divider />
            </div>
            {renderFooters()}
          </>
        }
      </Box>
    );
  };

  const renderProduct = () => {
    return (
      <Box color='default-2' variant='outlined' style={{ width, height }}>
        {renderChildren(CardMedia)}
        {renderChildren(CardContent)}
        {renderFooters()}
      </Box>
    );
  };

  const renderVideo = () => {
    return (
      <Box color='transparent' shadowSize='medium' variant='outlined' style={{ width, height }}>
        {renderChildren(CardMedia)}
        {renderChildren(CardContent)}
        {renderFooters()}
      </Box>
    );
  };

  const renderDefault = () => {
    return (
      <Box color='transparent' shadowSize='medium' style={{ width, height }}>
        <div className='flex'>
          <div className='flex flex-col justify-between w-full'>
            {renderChildren(CardContent)}
            {renderFooters()}
          </div>
          {renderChildren(CardMedia)?.length > 0 && (
            <div style={{ width: '45%' }}>
              {renderChildren(CardMedia)}
            </div>
          )}
        </div>
      </Box>
    );
  };

  const renderDefault2 = () => {
    return (
      <>
        {rotation === 'landscape' && (
          <Box color='default-2' variant='cleaned' style={{ width, height }}>
            <div
              className='rounded-large'
              style={{
                position: 'relative',
                paddingBottom: '75%',
                backgroundColor: 'black',
                backgroundImage: backgroundImage ? `url(${backgroundImage})` : '',
                backgroundPosition: 'center',
                backgroundRepeat: 'no-repeat',
                backgroundSize: "cover"
              }}
            >
              <div className='absolute bottom-0'>
                <Box>
                  {renderChildren(CardContent)}
                  {renderFooters()}
                </Box>
              </div>
            </div>
          </Box >
        )}
        {rotation === 'potrait' && (
          <Box color='default-2' variant='cleaned'>
            <div
              className='rounded-large'
              style={{
                position: 'relative',
                paddingBottom: '150%',
                backgroundColor: 'black',
                backgroundImage: backgroundImage ? `url(${backgroundImage})` : '',
                backgroundPosition: 'center',
                backgroundRepeat: 'no-repeat',
                backgroundSize: "cover"
              }}
            >
              <div className='absolute bottom-0'>
                <Box>
                  {renderChildren(CardContent)}
                  {renderFooters()}
                </Box>
              </div>
            </div>
          </Box >
        )}
      </>
    );
  };

  return (
    <>
      {variant === 'profile' && renderProfile()}
      {variant === 'product' && renderProduct()}
      {variant === 'video' && renderVideo()}
      {variant === 'default' && renderDefault()}
      {variant === 'default2' && renderDefault2()}
    </>
  );
};

Card.propTypes = {
  width: string,
  height: string,
  variant: oneOf(['profile', 'product', 'video', 'default', 'default2']),
  backgroundImage: string,                         // only for variant default2
  rotation: oneOf(['landscape', 'potrait']),  // only for variant default2

};

Card.defaultProps = {
  width: '',
  height: '',
  variant: 'default',
  backgroundImage: '',
  rotation: 'landscape'
};
