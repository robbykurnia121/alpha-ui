import React from 'react';
import Icon from '../../basic/Icon/Icon';
import {
  Card,
  CardTitle,
  CardSubtitle,
  CardBody,
  CardMedia,
  CardFooter,
  CardAction,
  CardContent,
  Image,
  Video,
  Text,
  Box,
  Button,
  Divider,
} from '../../';

export default {
  title: 'Container/Card',
  component: Card,
  argTypes: {
  },
};

const image = ({ src, aspectRatio, radius, alt = '' }) => {
  return (
    <Image src={src} aspectRatio={aspectRatio} radius={radius} />
  );
};

const video = ({ src, title }) => {
  return (
    <Video src={src} title={title} />
  );
};

const Template = (args) => (
  <Card {...args}>
    <CardContent>
      <CardTitle primary='Business • 10 min' primaryProps={{ color: 'disabled' }} />
      <CardSubtitle primary='Interior designers reveal 17 trends they wish would disappearwish' secondary='Dany Ashary' />
    </CardContent>
    <CardFooter>
      <CardBody>
        <Icon color='disabled' kind='send' />1
        <Icon color='disabled' kind='prev' />2
        <Icon color='disabled' kind='share' />2
      </CardBody>
    </CardFooter>
    <CardMedia
      component={Image}
      src='https://i.ibb.co/9v9xZJc/Rectangle-851.png'
      aspectRatio={1.7} />
  </Card>
);

export const Default = Template.bind({});

const Template2 = (args) => (
  <Card {...args}>
    <CardContent>
      <CardTitle
        primary='Business • 10 min'
        primaryProps={{ color: 'light', variant: 'body2' }} />
      <CardSubtitle
        primary='Interior designers reveal 17 trends they wish would disappearwish'
        secondary='Dany Ashary'
        primaryProps={{ color: 'light', variant: 'h5' }}
        secondaryProps={{ color: 'light', variant: 'body2' }}
      />
    </CardContent>
    <CardFooter
      primary={
        <>
          <Icon color='disabled' kind='send' />1
          <Icon color='disabled' kind='prev' />2
          <Icon color='disabled' kind='share' />2
        </>
      }
      primaryProps={{ color: 'disabled' }}
    />
  </Card>
);

export const Default2 = Template2.bind({});
Default2.args = {
  variant: 'default2',
  backgroundImage: 'https://i.ibb.co/GnBndCB/Rectangle-874.png'
};

const ProfileCardTemplate = (args) => (
  <Card variant='profile' {...args}>
    <CardContent>
      <CardTitle primary='Emery Donin' secondary='Web Designer' />
      <CardBody primary='Waktu respon' secondary='rata-rata 10 menit' />
      <Divider />
    </CardContent>

    <CardMedia
      component={image}
      src='https://i.ibb.co/9v9xZJc/Rectangle-851.png'
      aspectRatio={1} />
    <CardFooter secondary={<CardAction buttons={[{ label: 'send', kind: 'send' }]} />} />
    <CardAction buttons={[{ label: 'chat', color: 'primary' }]} />
  </Card>
);

export const ProfileCard = ProfileCardTemplate.bind({});

const VideoCardTemplate = (args) => (
  <Card variant='video' {...args}>
    <CardMedia
      component={Video}
      src="https://www.youtube.com/embed/tgbNymZ7vqY"
      title='video test' />
    <CardContent>
      <CardTitle
        primary='Interior designers reveal 17 trends they wish would disappearwis'
        secondary='Dany Ashary'
      />
    </CardContent>
  </Card>
);

export const VideoCard = VideoCardTemplate.bind({});

const ProductCardTemplate = (args) => (
  <Card variant='product'>
    <CardMedia
      component={image}
      src="https://i.ibb.co/2Mww7TQ/Rectangle-882-1.png"
      aspectRatio={1}
    />
    <CardContent>
      <CardTitle
        primary='New iMac 2021'
      />
      <CardSubtitle
        primary={(
          <div className='flex'>
            <Box variant='cleaned' color='primary'>
              <Text variant='body1'>-30%</Text>
            </Box>
            <Text variant='body1' color='disabled'>Rp25.000.000</Text>
          </div>
        )}
        secondary={<Text style={{ fontWeight: '600' }}>Rp22.490.000</Text>}
      />
    </CardContent>
    <CardFooter secondary='MOQ: 1000 Dozens' />
  </Card>
);

export const ProductCard = ProductCardTemplate.bind({});