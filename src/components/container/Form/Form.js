import React from 'react';
import { any, oneOf } from 'prop-types';
// import cn from 'classnames';

export default function Form({ children, header, shadowSize, padding, rounded }) {

  return (
    <div
    // className={cn(
    //   s[`shadow--${shadowSize}`],
    //   s[`${rounded}Rounded`])}
    >
      {header && <div c
      // lassName={cn(s.header, s[`header--${padding}Padding`])}
      >{header}</div>}
      <div
      // className={cn(s.children, s[`children--${padding}Padding`])}
      >{children}</div>
    </div>
  );
};

Form.propTypes = {
  children: any,
  header: any,
  shadowSize: oneOf(['none', 'm', 'l']),
  padding: oneOf(['none', 's', 'm', 'l']),
  rounded: oneOf(['none', 's', 'm', 'l', 'xl']),
};

Form.defaultProps = {
  children: null,
  header: null,
  shadowSize: 'none',
  padding: 'none',
  rounded: 'none',
};