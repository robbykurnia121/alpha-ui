import React from 'react';
import Form from './Form';
import {
  FormField
} from '../..';
import Content from '../Content';
import { Text } from '../../basic';

export default {
  title: 'Layout/Form',
  component: Form,
  argTypes: {
  },
};

const Template = (args) => <Form {...args} />;

export const Default = Template.bind({});
Default.args = {
};


const HeaderFormTemplate = (args) => (
  <Form {...args}>
    <FormField type="radio"
      label="Pilih Waktu"
      options={[
        { value: "now", label: "Sekarang" },
        { value: "later", label: "Nanti" },
      ]}
      inline
    />
    <FormField type="select"
      label="Paket"
      options={[
        { value: "1", label: "Chat (Rp 50.000)" },
        { value: "1", label: "Video Call (Rp 250.000)" },
      ]}
      inputProps={{
        getOptionLabel: (opt) => opt.label
      }}
    />
    <FormField type="radiopill"
      label="Pilih Tanggal"
      options={[
        { value: "1", label: "Senin, 16 Aug" },
        { value: "2", label: "Rabu, 18 Aug" },
        { value: "3", label: "Kamis, 19 Aug" },
        { value: "4", label: "Jumat, 20 Aug" },
      ]}
    />
    <FormField type="radiopill"
      label="Pilih Jam"
      options={[
        { value: "1", label: "10.00-12.00" },
        { value: "2", label: "12.00-14.00" },
        { value: "3", label: "14.00-16.00" },
        { value: "4", label: "16.00-18.00" },
      ]}
    />

  </Form>
);

export const HeaderForm = HeaderFormTemplate.bind({});
HeaderForm.args = {
  header: (
    <Content>
      <Text variant='h5'>Snack</Text>
      <Text>Show off your products with an Inspiring catalog design</Text>
    </Content>
  ),
  shadowSize: 'm',
  padding: 'l',
  rounded: 'm'
};

const ConsultingFormTemplate = (args) => (
  <Form {...args}>
    <FormField type="radio"
      label="Pilih Waktu"
      options={[
        { value: "now", label: "Sekarang" },
        { value: "later", label: "Nanti" },
      ]}
      inline
    />
    <FormField type="select"
      label="Paket"
      options={[
        { value: "1", label: "Chat (Rp 50.000)" },
        { value: "1", label: "Video Call (Rp 250.000)" },
      ]}
      inputProps={{
        getOptionLabel: (opt) => opt.label
      }}
    />
    <FormField type="radiopill"
      label="Pilih Tanggal"
      options={[
        { value: "1", label: "Senin, 16 Aug" },
        { value: "2", label: "Rabu, 18 Aug" },
        { value: "3", label: "Kamis, 19 Aug" },
        { value: "4", label: "Jumat, 20 Aug" },
      ]}
    />
    <FormField type="radiopill"
      label="Pilih Jam"
      options={[
        { value: "1", label: "10.00-12.00" },
        { value: "2", label: "12.00-14.00" },
        { value: "3", label: "14.00-16.00" },
        { value: "4", label: "16.00-18.00" },
      ]}
    />

  </Form>
);

export const ConsultingForm = ConsultingFormTemplate.bind({});
ConsultingForm.args = {
};

const QuotationFormTemplate = (args) => (
  <Form {...args}>
    <FormField type="radiopill"
      label="Kategori"
      options={[
        { value: "1", label: "Pakaian" },
        { value: "2", label: "Kecantikan" },
        { value: "3", label: "Kimia" },
        { value: "4", label: "Elektronik" },
        { value: "4", label: "Hadiah" },
        { value: "4", label: "Makanan & Minuman" },
        { value: "4", label: "Lain-lain" },
      ]}
    />
    <FormField type="text"
      label="Nama Produk"
      required
    />
    <FormField type="number"
      label="Jumlah"
      required
    />
    <FormField type="select"
      label="Satuan"
      options={[
        { value: "1", label: "Unit" },
        { value: "1", label: "Dus" },
      ]}
      inputProps={{
        getOptionLabel: (opt) => opt.label
      }}
    />
    <FormField type="checkbox"
      label="Fitur"
      options={[
        { value: "1", label: "Halal" },
        { value: "2", label: "ISO 999" },
        { value: "2", label: "Fitur ABC" },
        { value: "2", label: "Custom Packaging" },
        { value: "2", label: "Custom Logo" },
      ]}
      inline
    />

  </Form>
);

export const QuotationForm = QuotationFormTemplate.bind({});
QuotationForm.args = {
};