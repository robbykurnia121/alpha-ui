import React from 'react';
import Grid from './Grid';

export default {
  title: 'Container/Grid',
  component: Grid,
  argTypes: {
  },
};

const Template = (args) => <Grid {...args} />;

export const Default = Template.bind({});
Default.args = {
};