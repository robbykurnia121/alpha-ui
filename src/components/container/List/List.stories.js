import React from 'react';
import List from './List';
import { Default as ArticleCard } from '../Card/Card.stories.js'

export default {
  title: 'Container/List',
  component: List,
  argTypes: {
  },
};

const Template = (args) => (
  <List {...args}>
    <ArticleCard {...ArticleCard.args} />
    <ArticleCard {...ArticleCard.args} />
    <ArticleCard {...ArticleCard.args} />
    <ArticleCard {...ArticleCard.args} />
    <ArticleCard {...ArticleCard.args} />
    <ArticleCard {...ArticleCard.args} />
  </List>
);

export const Default = Template.bind({});
Default.args = {
};