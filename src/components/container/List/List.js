import React from 'react';
import { any, number, oneOf } from 'prop-types';

const space = {
  small: { spacing: '.25rem' },
  medium: { spacing: '.5rem' },
  large: { spacing: '1rem' }
};

export default function List({ columns, spacing, children, listRef }) {

  return (
    <div ref={listRef} className='flex flex-wrap'
      style={{
        marginLeft: `-${space[spacing]?.spacing}`,
        marginRight: `-${space[spacing]?.spacing}`,
        marginTop: `-${space[spacing]?.spacing}`,
        marginBottom: `-${space[spacing]?.spacing}`
      }}
    >
      {children?.length > 0 && children.map((child, index) => (
        <div key={index} className='columns'
          style={{
            width: ` ${100 / columns}%`,
            padding: `${space[spacing]?.spacing}`,
            position: 'relative',
          }}
        >
          {child}
        </div>
      ))}

    </div >
  );
};

List.propTypes = {
  listRef: any,
  columns: number,
  spacing: oneOf(['small', 'medium', 'large'])
};

List.defaultProps = {
  listRef: null,
  columns: 1,
  spacing: 'small'
};