import React from 'react';
import { Button } from '../../basic';
import Content from './Content';

export default {
  title: 'Layout/Content',
  component: Content,
  argTypes: {
  },
};

const Template = (args) => (
  <Content {...args} >
    <Button fullWidth color='primary' label='+ Keranjang'></Button>
    <Button fullWidth color='primary' label='Inquiry' variant='outlined'></Button>
  </Content>
);

export const Default = Template.bind({});
Default.args = {
};