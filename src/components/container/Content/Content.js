import React, { Children } from 'react';
import { bool, oneOf } from 'prop-types';
// import cn from 'classnames';

export default function Content({
  children, horizontal, between, gutterBottom, spacing, vertAlignment, horzAlignment, fullWidth
}) {
  const childrenWithProps = children && Children.toArray(children).map((child, index) => {
    if (React.isValidElement(child)) {
      return (
        <div style={child.props?.width ? { width: `${child.props?.width}%` } : {}}>
          {React.cloneElement(child, {
            key: index,
          })}
        </div>
      );
    }

    return child;
  });

  return (
    <div
    // className={cn(
    //   s.root,
    //   fullWidth && s.fullWidth,
    //   horizontal ? s.row : s.column,
    //   between && s.between,
    //   spacing && s.spacing,
    //   gutterBottom && s['gutter-bottom'],
    //   !between && s[`vert--${vertAlignment}`],
    //   s[`horz--${horzAlignment}`]
    // )}
    >
      {childrenWithProps}
    </div>
  );
};

Content.propTypes = {
  fullWidth: bool,
  gutterBottom: bool,
  horizontal: bool,
  between: bool,
  spacing: bool,
  horzAlignment: oneOf(['', 'left', 'center', 'right']),
  vertAlignment: oneOf(['', 'top', 'middle', 'bottom']),
};

Content.defaultProps = {
  fullWidth: false,
  gutterBottom: false,
  horizontal: false,
  between: false,
  spacing: true,
  horzAlignment: '',
  vertAlignment: '',
};