import React from 'react';
import Faqs from './Faqs';

export default {
  title: 'Landing/Faqs',
  component: Faqs,
  argTypes: {
  },
};

const Template = (args) => <Faqs {...args} />;

export const Default = Template.bind({});
Default.args = {
};