import React from 'react';
import Stats from './Stats';

export default {
  title: 'Landing/Stats',
  component: Stats,
  argTypes: {
  },
};

const Template = (args) => <Stats {...args} />;

export const Default = Template.bind({});
Default.args = {
};