import React from 'react';
import { any, arrayOf, string, bool, number, oneOf } from 'prop-types';
// import cn from 'classnames';
import { Box, Button, Text } from '../../basic';
import Section from '../../layout/Section';

export default function Features({
  images,
  title,
  subtitle,
  body,
  buttons,
  items,
  ...sectionProps
}) {

  return (
    <Section {...sectionProps}>
      {title && (
        <Text className='mt-6' variant='h1' style={{ fontWeight: '600' }}>{title}</Text>
      )}

      {subtitle && (
        <Text variant='h2' style={{ fontWeight: '600' }}>{subtitle}</Text>
      )}

      {body && (
        <Text variant='h3' style={{ fontWeight: '600' }}>{body}</Text>
      )}

      {buttons && buttons.map((button, index) => (
        <Button key={index} {...button} />
      ))}

      {items?.map((item, index) => (
        <div key={index} className='flex ml-4 text-center my-8'>
          <div
          // className={cn(s.numbering)}
          ><div>{index}</div></div>
          <div className='w-full'>
            <Box padding='large' shadowSize='medium' color='default' style={{ transform: "translateX(-24px)" }}>
              <Text variant='h5' style={{ fontWeight: '600' }}>{item}</Text>
            </Box>
          </div>
        </div>
      ))}
    </Section>
  );
};

Features.propTypes = {
  contentAlignment: oneOf(['left', 'center', 'right']),
  contentWidth: number,
  backgroudImage: string,
  images: arrayOf({}),
  title: any,
  subtitle: any,
  body: any,
  buttons: arrayOf({}),
  items: arrayOf({
    name: string,
    description: string,
    src: string
  }),
  // items: [{name, description, image/src, href, link}],

};

Features.defaultProps = {
  contentAlignment: 'left',
  contentWidth: 50,
  images: null,
  title: null,
  subtitle: null,
  body: null,
  buttons: null,
  items: null
};