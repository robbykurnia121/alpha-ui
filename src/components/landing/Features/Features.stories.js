import React from 'react';
import Features from './Features';

export default {
  title: 'Landing/Features',
  component: Features,
  argTypes: {
  },
};

const Template = (args) => <Features {...args} />;

export const Default = Template.bind({});
Default.args = {
  backgroundImage: 'https://i.ibb.co/Vqd8FBL/Rectangle-848-1.png',
  title: 'Kenapa harus Solubis?',
  items: ['Jutaan expert ada disini', 'Jutaan expert ada disini', 'Jutaan expert ada disini', 'Jutaan expert ada disini']
};