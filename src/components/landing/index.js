export { default as Faqs } from './Faqs';
export { default as Features } from './Features';
export { default as Hero } from './Hero';
export { default as Stats } from './Stats';
export { default as Testimonials } from './Testimonials';
