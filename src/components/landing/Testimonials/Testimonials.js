import React from 'react';
import { any, arrayOf, string, oneOf, bool, shape, number } from 'prop-types';
import { Avatar, Box, Divider, Image, Text, Video } from '../../basic';
import { Slider } from '../../container';
// import cn from 'classnames';

export default function Testimonials({
  slidesPerView,
  variant,
  images,
  title,
  subtitle,
  body,
  buttons,
  shadowSize,
  spaceBetween,
  items
}) {

  return (
    <div>
      <Slider
        loop={false}
        spaceBetween={spaceBetween}
        slidesPerView={slidesPerView}
        paginationType='hidden'
        shadowSize={shadowSize}
      >
        {items && items.map((item, index) => (
          <Box key={index} shadowSize={shadowSize} style={{ textAlign: 'left' }}>

            {item?.videoSrc && (
              <Video className='mb-4' src={item.videoSrc} title='video 1' />
            )}

            <Text variant='body2'>{item?.description}</Text>

            <div className='my-4'>
              <Divider />
            </div>

            <div className='flex items-center'>
              <Avatar size='s' src={item?.userAvatar} />
              <div className='ml-2'>
                <div>{item?.userName}</div>
                <div>{item?.userRole}</div>
              </div>
            </div>

          </Box>
        ))}
      </Slider>
    </div>
  );
};

Testimonials.propTypes = {
  shadowSize: oneOf(['none', 'medium', 'large']),
  spaceBetween: number,
  slidesPerView: number,
  background: string,
  variant: string,
  images: arrayOf(shape({})),
  title: any,
  subtitle: any,
  body: any,
  buttons: arrayOf(any),
  items: arrayOf(shape({
    name: string,
    description: string,
    image: string,
    videoSrc: string,
    href: string,
    link: any,
    userAvatar: any,
    userName: any,
    userRole: any
  }))
};

Testimonials.defaultProps = {
  shadowSize: 'none',
  spaceBetween: 16,
  slidesPerView: 2.5,
  background: '',
  variant: '',
  images: null,
  title: null,
  subtitle: null,
  body: null,
  buttons: null,
  items: null
};