import React from 'react';
import Testimonials from './Testimonials';

export default {
  title: 'Landing/Testimonials',
  component: Testimonials,
  argTypes: {
  },
};

const Template = (args) => <Testimonials {...args} />;

export const Default = Template.bind({});
Default.args = {
  // background: string,
  // variant: string,
  // images: arrayOf(shape({})),
  // title: any,
  // subtitle: any,
  // body: any,
  // buttons: arrayOf(any),
  items: [
    {
      // name: string,
      description: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nullam dignissim bibendum lorem id fermentum. A id risus dui tellus risus sit ut nibh. Nec elit varius enim, tempor nec, vehicula lacus.',
      image: '',
      videoSrc: 'https://www.youtube.com/embed/tgbNymZ7vqY',
      href: '',
      link: '',
      userAvatar: 'https://i.ibb.co/GnBndCB/Rectangle-874.png',
      userName: 'Ann Franci',
      userRole: 'Buyer'
    },
    {
      // name: string,
      description: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nullam dignissim bibendum lorem id fermentum. A id risus dui tellus risus sit ut nibh. Nec elit varius enim, tempor nec, vehicula lacus.',
      image: '',
      videoSrc: 'https://www.youtube.com/embed/tgbNymZ7vqY',
      href: '',
      link: '',
      userAvatar: 'https://i.ibb.co/GnBndCB/Rectangle-874.png',
      userName: 'Ann Franci',
      userRole: 'Buyer'
    },
    {
      // name: string,
      description: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nullam dignissim bibendum lorem id fermentum. A id risus dui tellus risus sit ut nibh. Nec elit varius enim, tempor nec, vehicula lacus.',
      image: '',
      videoSrc: 'https://www.youtube.com/embed/tgbNymZ7vqY',
      href: '',
      link: '',
      userAvatar: 'https://i.ibb.co/GnBndCB/Rectangle-874.png',
      userName: 'Ann Franci',
      userRole: 'Buyer'
    },
  ]
};