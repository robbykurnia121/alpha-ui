import React from 'react';
import { AddIcon, CancelIcon, CommentIcon, SearchIcon } from '../../../icons';
import { Button, NavItem } from '../../basic';
import Hero from './Hero';

export default {
  title: 'Landing/Hero',
  component: Hero,
  argTypes: {
  },
};

const Link = ({ children, href }) => {
  return (
    <a href={href}>
      {children}
    </a>
  );
};

const NavProps = {
  boxProps: {
    shadowSize: 'medium',
    variant: 'outlined',
    color: 'default',
  },
  align: 'left',
  variant: 'icon',
  itemSize: 'xlarge',
  iconProps: {
    size: 'xlarge'
  },
  titleProps: {
    maxLine: 1,
    className: 'text-left',
    style: { fontWeight: '600' },
    variant: 'body2',
    show: true
  },
  subtitleProps: {
    maxLine: 2,
    className: 'text-left',
    variant: 'body2',
    show: true
  }
};

const Template = (args) => <Hero {...args} />;

export const Default = Template.bind({});
Default.args = {
  backgroundImage: 'https://i.ibb.co/87xPdxq/shutterstock-542281291-1-1.png',
  title: 'Get Your Right Solution For Your Business',
  subtitle: 'We’ll start consulting’s definition accoring to the Oxford Dictinary Consulting means “engaged in business of giving expert advice.',
  contentWidth: 50,
  height: 80,
  vertAlignment: "bottom",
  horzAlignment: "left",
  slidesPerView: 6.5,
  navItems: [
    <NavItem {...NavProps} title="Find the expert" subtitle='Consulting with your favourite expert' icon={SearchIcon} isActive={true} href='/' Link={Link} />,
    <NavItem {...NavProps} title="Business Insight" subtitle='Explore your business idea & innovation with our expertise' icon={AddIcon} isActive={false} href='/expert' Link={Link} />,
    <NavItem {...NavProps} title="Business Matching" subtitle='Creating Project, Benchmarking' icon={CommentIcon} isActive={false} href='/expert' Link={Link} />,
    <NavItem {...NavProps} title="Marketplace" subtitle='Find and buy your product needs' icon={CancelIcon} isActive={false} href='/expert' Link={Link} />,
    <NavItem {...NavProps} title="Solubis Academy" subtitle='Learning new skill with Solubis Expert' icon={CancelIcon} isActive={false} href='/rfq' />,
    <NavItem {...NavProps} title="Services" subtitle='Learning new skill with Solubis Expert' icon={CancelIcon} isActive={false} href='/rfq' />,
    <NavItem {...NavProps} title="Services" subtitle='Learning new skill with Solubis Expert' icon={CancelIcon} isActive={false} href='/rfq' />
  ]
};

export const Default2 = Template.bind({});
Default2.args = {
  contentWidth: 100,
  color: "primary",
  fixed: true,
  images: [{ src: 'https://i.ibb.co/dmT5f6D/Rectangle-850.png' }],
  title: 'Get Your Right Solution For Your Business',
  body: 'Amet minim mollit non deserunt ullamco est sit aliqua dolor do amet sint. Velit officia consequat duis enim velit mollit. Exercitation veniam consequat sunt nostrud amet.Amet minim mollit non deserunt ullamco est sit aliqua dolor do amet sint. Velit officia consequat duis enim velit mollit. Exercitation veniam consequat sunt nostrud amet.',
  buttons: [<Button variant='outlined' rounded='rounded-full' color='secondary' label='Join us and Become Expertise' />],
};