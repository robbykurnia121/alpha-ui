import React from 'react';
import { any, arrayOf, bool, number, oneOf, string } from 'prop-types';
import cn from 'classnames';
import { Button, Text } from '../../basic';
import { Slider } from '../../container';

import { useMediaQuery } from '../../../libs';
import Section from '../../layout/Section';

export default function Hero({
  heroRef,
  reverse,
  variant,
  images,
  textColor,
  title,
  subtitle,
  body,
  buttons,
  navItems,
  slidesPerView,
  spaceBetween,
  shadowSize,
  contentWidth,
  ...sectionProps
}) {

  const mdUp = useMediaQuery('md-up');

  // Note: We'll add contentWidth manually since we want to use full width for
  // navItems

  return (
    <Section {...sectionProps}>
      <div
        className={cn(
          "w-full",
          !mdUp && 'flex-col',
          images && 'items-center justify-center',
          reverse && 'flex-row-reverse',
          'flex')}
        style={{ width: `${contentWidth}%` }}
      >
        {images && images.map((image, index) => (
          <div key={index} className={mdUp ? 'w-1/2' : 'w-full'}>
            <img alt='' src={image.src} width='100%' />
          </div>
        ))}

        {images && <div className='mx-4'></div>}

        <div className={cn(
          images && mdUp && 'w-1/2',
          images && !mdUp && 'w-full'
        )}>
          {title && (
            <Text variant='h1' color={textColor} gutterBottom className='mt-4 mb-2'>{title}</Text>
          )}

          {subtitle && (
            <Text variant='h5' color={textColor} gutterBottom>{subtitle}</Text>
          )}

          {body && (
            <Text variant='body2' color={textColor} gutterBottom>{body}</Text>
          )}

          {buttons && (
            <div
            // className={cn(s[`button--${sectionProps.contentAlignment}`], 'flex items-center mt-2')}
            >
              {buttons.map((button, index) => (
                <Button key={index} className='my-2' {...button} />
              ))}
            </div>
          )}
        </div>

      </div>
      <div className={cn("w-full")}>

        {navItems && slidesPerView > 0 && (
          <div>
            <Slider
              shadowSize={shadowSize}
              loop={false}
              spaceBetween={spaceBetween}
              slidesPerView={slidesPerView}
              paginationType='hidden'
              data={navItems}
            />
          </div>
        )}
        {navItems && slidesPerView === 0 && (
          <div className='flex flex-wrap' style={{ margin: '0px -8px' }}>
            {navItems.map((nav, index) => (
              <div style={{ width: '33.3%', padding: '8px' }} key={index}>{nav}</div>
            ))}
          </div>
        )}
      </div>
    </Section>
  );

  // return (
  //   // div wraper replace with section
  //   <Section
  //     {...sectionProps}
  //   >
  //     <div style={{
  //       textAlign: contentAlignment,
  //       display: 'flex',
  //       height: '100%',
  //       flexDirection: 'column',
  //       justifyContent: 'space-between',
  //     }}>

  //     </div>
  //   </Section>
  // );
};

Hero.propTypes = {
  heroRef: any,
  reverse: bool,
  variant: string,
  images: arrayOf(any),
  title: any,
  subtitle: any,
  body: any,
  buttons: arrayOf(any),
  navItems: arrayOf(any),
  slidesPerView: number,
  spaceBetween: number,
  shadowSize: oneOf(['none', 'medium', 'large']),
  contentAlignment: oneOf(['left', 'center', 'right']),
  horzAlignment: oneOf(['left', 'center', 'right']),
  vertAlignment: oneOf(['top', 'middle', 'bottom']),
  contentWidth: number,
  height: number,
};

Hero.defaultProps = {
  heroRef: null,
  reverse: false,
  color: 'default',
  variant: '',
  images: null,
  title: null,
  subtitle: null,
  body: null,
  buttons: null,
  navItems: null,
  slidesPerView: null,
  spaceBetween: 16,
  shadowSize: 'none',
};