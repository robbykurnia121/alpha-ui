import React from 'react';
import { any } from 'prop-types';

export default function BottomNavbar({ primaryNav }) {

  return (
    <div>
      {primaryNav}
    </div>
  );
};

BottomNavbar.propTypes = {
  primaryNav: any
};

BottomNavbar.defaultProps = {
  primaryNav: null
};