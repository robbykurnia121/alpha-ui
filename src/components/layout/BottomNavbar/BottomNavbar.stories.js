import React from 'react';
import BottomNavbar from './BottomNavbar';

export default {
  title: 'Layout/BottomNavbar',
  component: BottomNavbar,
  argTypes: {
  },
};

const Template = (args) => <BottomNavbar {...args} />;

export const Default = Template.bind({});
Default.args = {
};