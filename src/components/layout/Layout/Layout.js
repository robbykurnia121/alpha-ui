import React, { useRef } from 'react';
import { any, bool } from 'prop-types';
// import s from './Layout.module.css';
import cn from 'classnames';
import { useUI } from '../../../shared/context';

export default function Layout({
  navbar, mobileNavbar, bottomNavbar, isMobile, footer, children
}) {
  const navbarRef = useRef(null);
  const { overlayNavbar, setOverlayNavbar } = useUI();


  return (
    <div
      className={cn(s.root)}>
      <div ref={navbarRef}
        // className={cn(s.header, !isMobile && s.boxShadow)} 
        style={{ backgroundColor: 'white' }}>
        {isMobile ? mobileNavbar : navbar}
      </div>

      {overlayNavbar && (
        <div onClick={() => setOverlayNavbar(false)}
        // className={cn(s.dimNavbar)}
        ></div>
      )}

      <main
      // className={cn(s.body, 'fit')}
      >
        {children}
      </main>

      {/*
        <Footer pages={pageProps.pages} />

        <Sidebar open={displaySidebar} onClose={closeSidebar}>
          <CartSidebarView />
        </Sidebar>
        */}

      {/*}
      <Modal
        className={`w-full ${modalViewProps?.useNonFullHeight ? 'self-end' : 'h-full'}`}
        customPadding="p-0"
        noButtonClose={true}
        open={displayModal}
        onClose={closeModal}
        placeOnVeryTop={modalViewProps?.placeOnVeryTop ? true : false}
      >
        {modalView === 'CART_VIEW' && <CartView onClose={closeModal} {...(modalViewProps || {})} />}
        {modalView === 'CHAT_VIEW' && <ChatListView onClose={closeModal} {...(modalViewProps || {})} />}
        {modalView === 'SEARCH_VIEW' && <SearchView onClose={closeModal} {...(modalViewProps || {})} />}
        {modalView === 'LOGIN_VIEW' && <LoginView onClose={closeModal} {...(modalViewProps || {})} />}
        {modalView === 'REGISTER_VIEW' && <RegisterView onClose={closeModal} {...(modalViewProps || {})} />}
        {modalView === 'SHARE_VIEW' && <ShareView onClose={closeModal} {...(modalViewProps || {})} />}
        {modalView === 'PRODUCT_DESCRIPTION_VIEW' && <ProductDescription onClose={closeModal} {...(modalViewProps || {})} />}
        {modalView === 'PRODUCT_REVIEW_LIST_VIEW' && <ProductReviewList onClose={closeModal} {...(modalViewProps || {})} />}
      </Modal>
      <Toast />      
      */}

      <div
      // className={cn(s.bottomNavbar, 'fixed bottom-0')}
      >
        {bottomNavbar}
      </div>


      {footer && (
        <footer style={{ background: '#FAFAFA' }}>
          <div
          // className={cn(s.footer)}
          >
            {footer}
          </div>
        </footer>
      )}
    </div>
  );
};

Layout.propTypes = {
  navbar: any,
  mobileNavbar: any,
  bottomNavbar: any,
  children: any,
  isMobile: bool,
  footer: any
};

Layout.defaultProps = {
  navbar: null,
  mobileNavbar: null,
  bottomNavbar: null,
  children: null,
  isMobile: false,
  footer: null
};
