import React from 'react';
import { AddIcon, CancelIcon, CommentIcon, SearchIcon } from '../../../icons';
import Layout from './Layout';
import {
  Default as Body,
  Footer
} from '../SectionGroup/SectionGroup.stories'
import {
  ConsultingForm,
  QuotationForm,
} from '../../container/Form/Form.stories'
import {
  SectionGroup,
  Section,
  Image,
  Card, CardMedia, CardContent, CardTitle, CardBody, CardFooter, CardAction,
  Button,
  Text,
  Divider,
  List,
  Nav, NavItem,
} from '../..';

export default {
  title: 'Layout/Layout',
  component: Layout,
  argTypes: {
  },
};

const Link = ({ children, href, ...props }) => {
  return (
    <a {...props} href={href}>
      {children}
    </a>
  );
};

const bottomNavbarProps = {
  variant: 'icon',
  fullWidth: true,
  boxProps: {
    variant: 'cleaned',
    color: 'default'
  },
  titleProps: {
    variant: 'body2'
  }
};

const Template = (args) => (
  <Layout {...args}>
    <Body {...Body.args} fixed />
    <Footer {...Footer.args} fixed />
  </Layout>
);

export const HomePage = Template.bind({});
HomePage.args = {
  bottomNavbar: (
    <Nav {...bottomNavbarProps}>
      <NavItem title="Beranda" subtitle='Subtitle 1' icon={SearchIcon} isActive={true} href='/' Link={Link} />
      <NavItem title="Riwayat" subtitle='Subtitle 2' icon={AddIcon} isActive={false} href='/expert' Link={Link} />
      <NavItem title="Pusat Bantuan" subtitle='Subtitle 3' icon={CommentIcon} isActive={false} href='/expert' Link={Link} />
      <NavItem title="Tentang Kami" subtitle='Subtitle 4' icon={CancelIcon} isActive={false} href='/expert' Link={Link} />
      <NavItem title="Tentang" subtitle='Subtitle 5' icon={CancelIcon} isActive={false} href='/rfq' />
    </Nav>
  )
};

const ConsultingOrderFormTemplate = (args) => (
  <Layout {...args}>
    <SectionGroup width={50}>
      <Section title="Permintaan Konsultasi" variant="h3" padding="small">
        <Text variant="body1" className="font-bold mb-3">Expert</Text>

        <Card variant='profile'>
          <CardMedia
            component={Image}
            src='https://i.ibb.co/9v9xZJc/Rectangle-851.png'
            size='large'
            aspectRatio={1} />
          <CardContent>
            <CardTitle 
              primary='Ruben Ruben' 
              secondary='Web Designer' 
              primaryProps={{ variant: "h6" }} 
            />
          </CardContent>
        </Card>
      </Section>

      <Section title="Jadwal Konsultasi" variant="h4" padding="small">
        <ConsultingForm {...ConsultingForm.args} />
      </Section>
      <Section title="Promosi" variant="h4" padding="small">
      </Section>
      <Section title="Check Out" variant="h4" padding="small">
        <div className="mb-10">
          <Text variant="body1" className="font-bold mb-3">Metode Pembayaran</Text>        
          <div className="flex flex-col space-y-2">
            <div className="grid grid-cols-2 gap-2 items-baseline">
              <Text>Bank Transfer</Text>
              <Text variant="body1" className="font-bold text-right">BCA</Text>
            </div>
          </div>
        </div>

        <div className="mb-10">
          <Text variant="body1" className="font-bold mb-3">Ringkasan Pesanan</Text>

          <div className="flex flex-col space-y-2">
            <div className="grid grid-cols-2 gap-2 items-baseline">
              <Text>2 Jam Sesi</Text>
              <Text variant="body1" className="font-bold text-right">Rp 1.200.000</Text>
            </div>
            <div className="grid grid-cols-2 gap-2 items-baseline">
              <Text>Potongan Harga 10%</Text>
              <Text variant="body1" className="font-bold text-right text-red">-Rp 120.000</Text>
            </div>          
          </div>
          <div className="mt-4 mb-4">
            <Divider />
          </div>
          <div className="flex flex-col space-y-2">
            <div className="grid grid-cols-2 gap-2 items-baseline">
              <Text>Total</Text>
              <Text variant="h6" className="text-right">Rp 1.080.000</Text>
            </div>
          </div>
        </div>
        <div className="mt-4">
          <Button title="Bayar" variant="contained" color="primary" size="large" fullWidth />
        </div>
      </Section>
    </SectionGroup>
  </Layout>
);

export const ConsultingOrderForm = ConsultingOrderFormTemplate.bind({});
ConsultingOrderForm.args = {
};

const QuotationRequestTemplate = (args) => (
  <Layout {...args}>
    <SectionGroup width={50}>
      <Section title="Permintaan Quotation" variant="h3">
        <Section title="Spesifikasi Produk" variant="h4">
          <QuotationForm {...QuotationForm.args} />
          <div className="mt-12">
            <Button title="Lanjut" variant="contained" color="primary" size="large" fullWidth />
          </div>          
        </Section>
      </Section>        
    </SectionGroup>
  </Layout>
);

export const QuotationRequest = QuotationRequestTemplate.bind({});
QuotationRequest.args = {
};

const ConsultationTabs = () => (
  <Nav 
    variant='tab2'
    boxProps={{ color: 'default', padding: 'medium', variant: 'contained' }}
    titleProps={{ color: 'primary', colorInactive: 'disabled' }}
    >
    <NavItem title="Semua" />
    <NavItem title="Menunggu Pembayaran" />
    <NavItem title="Aktif" isActive />
  </Nav>
);

const ConsultationCard = () => (
  <Card variant='profile'>
    <CardMedia
      component={Image}
      src='https://i.ibb.co/9v9xZJc/Rectangle-851.png'
      size='large'
      aspectRatio={1} />
    <CardContent>
      <CardTitle 
        primary='Ruben Ruben' 
        secondary='Web Designer' 
        primaryProps={{ variant: "h6" }} 
      />
      <CardBody
        primary='Rp 1.800.000'
      />
    </CardContent>  
    <CardFooter>
      <Text variant="body2" color="secondary">Ruben sedang di ruang konsultasi</Text>
      <CardAction buttons={[
          { label: 'Lapor', color: 'primary', variant: 'link', size: 'small' },        
          { label: 'Masuk ke Ruang Konsultasi', color: 'primary' }
        ]} 
        />
    </CardFooter>
  </Card>
);

const ConsultationHistoryTemplate = (args) => (
  <Layout {...args}>
    <SectionGroup width={50}>
      <Section title="Riwayat Konsultasi" variant="h3">
        <Section padding="none">
          <ConsultationTabs />
        </Section>
        <Section padding="none">
          <List>
            <ConsultationCard />
            <ConsultationCard />
            <ConsultationCard />
            <ConsultationCard />
          </List>        
        </Section>
      </Section>        
    </SectionGroup>
  </Layout>
);

export const ConsultationHistory = ConsultationHistoryTemplate.bind({});

