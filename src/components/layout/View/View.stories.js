import React from 'react';
import View from './View';

export default {
  title: 'Layout/View',
  component: View,
  argTypes: {
  },
};

const Template = (args) => <View {...args} />;

export const Default = Template.bind({});
Default.args = {
};