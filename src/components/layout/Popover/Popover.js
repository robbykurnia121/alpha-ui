import React from 'react';
import { any, bool, func, oneOf, string } from 'prop-types';
import Modal from '../../basic/Modal';

export default function Popover({ show, onClose, children, from, size, variant, parentRef, widthParent, half, ...props }) {

  return (
    <Modal
      parentRef={parentRef}
      onClose={onClose}
      show={show}
      from={from}
      variant={variant}
      popoverSize={size}
      widthParent={widthParent}
      maxWidth={half ? '80%' : '100%'}
      {...props}>
      {children}
    </Modal>
  );
};

Popover.propTypes = {
  show: bool.isRequired,
  widthParent: bool,
  variant: string,
  children: any,
  onClose: func,
  size: oneOf(['small', 'medium', 'large', 'fullscreen']),
  half: bool
};

Popover.defaultProps = {
  variant: 'popover',
  widthParent: false,
  onClose: null,
  children: null,
  size: 'small',
  half: false
};