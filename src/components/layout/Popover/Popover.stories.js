import React, { useState } from 'react';
import Popover from './Popover';

export default {
  title: 'Layout/Popover',
  component: Popover,
  argTypes: {
  },
};

const Template = (args) => {
  const [show, setShow] = useState(false);
  const [show2, setShow2] = useState(false);
  const [show3, setShow3] = useState(false);
  const [show4, setShow4] = useState(false);

  return (
    <div>

      <div onClick={() => setShow(!show)}>Small</div>
      <Popover {...args} show={show} onClose={() => setShow(!show)} size='small'>
        <div>Test Test</div>
      </Popover>

      <div onClick={() => setShow2(!show2)}>Medium</div>
      <Popover {...args} show={show2} onClose={() => setShow2(!show2)} size='medium'>
        <div>Test Test</div>
      </Popover>

      <div onClick={() => setShow3(!show3)}>Large</div>
      <Popover {...args} show={show3} onClose={() => setShow3(!show3)} size='large'>
        <div>Test Test</div>
      </Popover>

      <div onClick={() => setShow4(!show4)}>Large</div>
      <Popover {...args} show={show4} onClose={() => setShow4(!show4)}>
        <div>Test Test</div>
      </Popover>
    </div>
  );
};

export const Default = Template.bind({});
Default.args = {
};