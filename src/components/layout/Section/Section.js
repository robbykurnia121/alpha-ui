import React from 'react';
import { any, bool, number, oneOf, string } from 'prop-types';
import Text from '../../basic/Text/Text';
// import s from './Section.module.css';
import cn from 'classnames';
import { arrayOf, shape } from 'prop-types';
import { Button } from '../../basic';

export default function Section({
  // data 
  title, subtitle, children, buttons,

  // styling
  variant, color, backgroundImage, backgroundCover, width, height, className, fixed, padding,
  contentAlignment, contentWidth, topRounded,
  horzAlignment, vertAlignment,
}) {
  return (
    <div
      className={cn(
        s[`backgroundColor--${color}`],
      )}
    >
      <div
        className={cn(
          'w-full',
          // fixed && s.fixed,
          // backgroundImage && s.backgroundImage,
          // backgroundCover && s.backgroundCover,
          // s[`backgroundColor--${color}`],
          className
        )}
        style={{
          backgroundImage: backgroundImage ? `url(${backgroundImage})` : '',
        }}
      >
        <div className={cn(
          // s.root,
          // fixed && s.fixed,
          // s[`padding--${padding}`],
          // s[`vert--${vertAlignment || 'left'}`],
          // s[`horz--${horzAlignment || 'middle'}`],
        )}
          style={{
            height: typeof height === 'number' ? `${height}vh` : '',
          }}
        >
          <div className={cn(
            // s.content,
            // s[`content--${contentAlignment}`]
          )}
            style={{ width: `${contentWidth || 100}%` }}
          >
            {(title || subtitle || buttons) && (
              <div className={cn(
                // s.header,
                'flex')}
              >
                {title && (
                  <Text variant={variant}>
                    {title}
                  </Text>
                )}
                {subtitle && (
                  <div className='ml-auto'>
                    {subtitle}
                  </div>
                )}
                {buttons && (
                  <div className='flex ml-auto whitespace-nowrap' style={{ height: 'max-content' }}>
                    {buttons.map((button, index) => (
                      <Button
                        variant="contained"
                        color="primary-2"
                        {...button}
                        key={index} />
                    ))}
                  </div>
                )}
              </div>
            )}
            {children}
          </div>
        </div>
      </div>
    </div>
  );
};

Section.propTypes = {
  backgroundImage: string,
  backgroundCover: bool,
  color: oneOf(['default', 'primary', 'secondary', 'light', 'disabled', 'inactive']),
  variant: oneOf(['h1', 'h2', 'h3', 'h4', 'h5', 'h6', 'body1', 'body2']),
  title: any,
  subtitle: any,
  buttons: arrayOf(shape({})),
  navbar: bool,
  verticalPadding: bool,
  width: number,
  height: number,
  fixed: bool,
  padding: oneOf(['none', 'normal', 'large']),
  contentAlignment: oneOf(['left', 'center', 'right']),
  horzAlignment: oneOf(['left', 'center', 'right']),
  vertAlignment: oneOf(['top', 'middle', 'bottom']),
  contentWidth: number,
};

Section.defaultProps = {
  backgroundImage: '',
  backgroundCover: false,
  color: 'default',
  variant: 'h3',
  title: null,
  subtitle: null,
  buttons: null,
  navbar: false,
  verticalPadding: true,
  width: null,
  fixed: false,
  padding: 'normal',
  contentAlignment: 'left',
  contentWidth: 100,
  horzAlignment: 'left',
  vertAlignment: 'middle',
};