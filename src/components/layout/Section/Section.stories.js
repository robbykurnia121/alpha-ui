import React from 'react';
import Section from './Section';
import { ProfileSlider } from '../../container/Slider/Slider.stories'

export default {
  title: 'Layout/Section',
  component: Section,
  argTypes: {
  },
};

const Template = (args) => (
  <Section {...args}>
    <ProfileSlider {...ProfileSlider.args} />
  </Section>
);

export const Default = Template.bind({});
Default.args = {
  variant: 'h2',
  title: "Expert Teratas",
  subtitle: 'Lihat Semua',
};

export const Fixed = Template.bind({});
Fixed.args = {
  variant: 'h2',
  title: "Expert Teratas",
  subtitle: 'Lihat Semua',
  fixed: true,
};