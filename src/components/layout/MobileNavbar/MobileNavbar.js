import React from 'react';
import { any, string } from 'prop-types';
// import s from './MobileNavbar.module.css';
import { Image } from '../../basic';

export default function MobileNavbar({
  backgroundColor,
  logo,
  primaryNav,
  secondaryNav,
  searchbar,
}) {

  return (
    <div
      // className={s.root}
      style={{ backgroundColor }}>
      <div style={{
        minWidth: "33%",
        display: 'flex',
        justifyContent: 'start'
      }}>
        {primaryNav}
      </div>
      {logo &&
        <Image image={logo} />
      }
      <div style={{
        minWidth: '33%',
        display: 'flex',
        justifyContent: 'flex-end',
      }}>
        {secondaryNav}
      </div>
    </div>
  );
};

MobileNavbar.propTypes = {
  backgroundColor: string,
  logo: string,
  primaryNav: any,
  secondaryNav: any,
  searchbar: any,
};

MobileNavbar.defaultProps = {
  backgroundColor: 'white',
  logo: '',
  primaryNav: null,
  secondaryNav: null,
  searchbar: null,
  mobileNav: null,
  isMobile: false
};