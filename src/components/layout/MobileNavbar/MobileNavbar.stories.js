import React from 'react';
import MobileNavbar from './MobileNavbar';
import {
  Nav,
  NavItem,
} from '../../basic';

export default {
  title: 'Layout/MobileNavbar',
  component: MobileNavbar,
  argTypes: {
  },
};

const PrimaryNav = () => (
  <Nav>
    <NavItem label="Home" />
    <NavItem label="Products" />
    <NavItem label="Solutions" />
    <NavItem label="About" />
  </Nav>
);

const SecondaryNav = () => (
  <Nav>
    <NavItem label="Notifications" />
    <NavItem label="Chat" />
    <NavItem label="Accounts" />
  </Nav>
);

const Template = (args) => (
  <MobileNavbar {...args}
    primaryNav={<PrimaryNav />}
    secondaryNav={<SecondaryNav />}
  >
  </MobileNavbar>
);

export const Default = Template.bind({});
Default.args = {
};