import React, { Children } from 'react';
import { any, bool, number, oneOf } from 'prop-types';
import cn from 'classnames';
// import s from './SectionGroup.module.css';

export default function SectionGroup({
  children, fixed, horizontal, removePadding,
}) {
  const childrenWithProps = children && Children.toArray(children).map((child, index) => {
    if (React.isValidElement(child)) {
      return (
        <div
          className={cn(
            "w-full",
            // child.props?.topRounded && s.topRounded
          )}
          style={{ width: child.props?.width ? `${child.props?.width}%` : '' }}>
          {React.cloneElement(child, {
            key: index,
            fixed
          })}
        </div>
      );
    }

    return child;
  });

  return (
    <div
      className={cn(
        // s.root,
        // removePadding && s.removePadding,
        // horizontal ? s.row : s.column,
      )}
    >
      {childrenWithProps}
    </div>
  );

};

SectionGroup.propTypes = {
  variant: oneOf(['h1', 'h2', 'h3', 'h4', 'h5', 'h6', 'body1', 'body2']),
  title: any,
  subtitle: any,
  body: any,
  fixed: bool,
  width: number,
  horizontal: bool,
  topRounded: bool,
};

SectionGroup.defaultProps = {
  variant: 'h2',
  title: null,
  subtitle: null,
  body: null,
  fixed: false,
  width: null,
  horizontal: false,
  topRounded: false,
};