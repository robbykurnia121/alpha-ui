import React from 'react';
import {
  Section,
  SectionGroup,
  Text,
} from '../..';

import { ProfileSlider } from '../../container/Slider/Slider.stories';
import { Default as ArticleList } from '../../container/List/List.stories';
import { Default as FeaturesSection } from '../../landing/Features/Features.stories';

import {
  Default as MainHero,
  Default2 as SolutionHero
} from '../../landing/Hero/Hero.stories';


export default {
  title: 'Layout/SectionGroup',
  component: SectionGroup,
  argTypes: {
  },
};

const DefaultTemplate = (args) => (
  <div className="w-full">
    <SectionGroup {...args}>
      <MainHero {...MainHero.args} />
      <Section title="Expert Teratas" buttons={[{ label: "View All" }]}>
        <ProfileSlider {...ProfileSlider.args} />
      </Section>
      <Section title="Insight">
        <SectionGroup horizontal>
          <Section variant="h4" padding="none" title="Artikel Terbaru" buttons={[{ label: "View All" }]} width={30}>
            <ArticleList {...ArticleList.args} />
          </Section>
          <Section variant="h4" padding="none" title="Video Terbaru" buttons={[{ label: "View All" }]}>
            <ArticleList {...ArticleList.args} />
          </Section>
          <Section variant="h4" padding="none" title="Video Terbaru" buttons={[{ label: "View All" }]} width={30}>
            <ArticleList {...ArticleList.args} />
          </Section>
        </SectionGroup>
      </Section>
      <SolutionHero {...SolutionHero.args} />
      <FeaturesSection {...FeaturesSection.args} />
    </SectionGroup>
  </div>
);

export const Default = DefaultTemplate.bind({});
Default.args = {
  fixed: true
};

const HorizontalTemplate = (args) => (
  <SectionGroup horizontal {...args}>
    <Section variant="h5" title="Artikel Terbaru">
      Artikel
    </Section>
    <Section variant="h5" title="Video Terbaru">
      Video
    </Section>
  </SectionGroup>
);

export const Horizontal = HorizontalTemplate.bind({});
Horizontal.args = {
};

const FooterTemplate = (args) => (
  <Section fixed>
    <SectionGroup horizontal >
      <Section padding='none'>
        Logo
      </Section>
      <Section title='Solubis' variant='body1'>
        <Text variant='body2'>Tentang</Text>
        <Text variant='body2'>Hubungi Kami</Text>
        <Text variant='body2'>Kebijakan Privasi</Text>
        <Text variant='body2'>Penghargaan Solubis</Text>
        <Text variant='body2'>Pusat Bantuan</Text>
      </Section>
      <Section title='SOL-X' variant='body1'>
        <Text variant='body2'>Cara Daftar</Text>
        <Text variant='body2'>Komisi</Text>
        <Text variant='body2'>Syarat dan Ketentuan</Text>
      </Section>
      <Section title='B2B Partner' variant='body1'>
        <Text variant='body2'>Cara Daftar</Text>
        <Text variant='body2'>Syarat dan Ketentuan</Text>
      </Section>
      <Section>
        Play Store, Apple Store
        <Text variant="h6">Ikuti Kami Disini"</Text>
        IG
      </Section>
    </SectionGroup>
    <SectionGroup horizontal>
      <Section title='© 2021 SOLUBIS' variant='body1' width={1 / 6} />
      <Section title='Metode Pembayaran' variant='body1'>
        Payment List
      </Section>
    </SectionGroup>
  </Section>
);

export const Footer = FooterTemplate.bind({});
Footer.args = {
  fixed: true,
};