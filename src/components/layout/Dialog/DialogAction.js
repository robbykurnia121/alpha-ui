import React from "react";
import { any, arrayOf } from "prop-types";
import { Button } from "../../basic";

export default function DialogAction({ buttons }) {
  return (<div>{buttons.map((button, index) => (
    <Button key={index} className='my-2' {...button} />
  ))}</div>);
};
DialogAction.propTypes = {
  buttons: arrayOf(any),
};

DialogAction.defaultProps = {
  buttons: null,
};