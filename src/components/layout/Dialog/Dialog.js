import React, { Children, isValidElement, cloneElement } from 'react';
import DialogBody from './DialogBody';
import DialogHeader from './DialogHeader';
import DialogFooter from './DialogFooter';
import Popover from '../Popover';
import { bool, oneOf } from 'prop-types';

export default function Dialog({ show, onClose, children, from, variant, parentRef, size, widthParent, half, ...props }) {
  const renderChildren = (component) => {
    return Children.map(children, e => {
      if (isValidElement(e) && e.type === component) {
        return cloneElement(e, {});
      }
    });
  };
  return (
    <Popover
      variant={variant}
      parentRef={parentRef}
      show={show}
      onClose={onClose}
      size={size}
      from={from}
      half={half}
      widthParent={widthParent}
      {...props}>
      {renderChildren(DialogHeader)}
      {renderChildren(DialogBody)}
      {renderChildren(DialogFooter)}
    </Popover>
  );
};

Dialog.propTypes = {
  size: oneOf(['small', 'medium', 'large', 'fullscreen']),
  widthParent: bool,
  half: bool
};

Dialog.defaultProps = {
  size: 'small',
  widthParent: false,
  half: false
};