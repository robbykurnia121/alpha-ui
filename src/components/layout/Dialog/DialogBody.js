import React from 'react';
// import cn from 'classnames';

export default function DialogBody({ children }) {

  return (
    <div
    // className={cn(s.body)}
    >
      {children}
    </div>
  );
};

DialogBody.propTypes = {
};

DialogBody.defaultProps = {
};