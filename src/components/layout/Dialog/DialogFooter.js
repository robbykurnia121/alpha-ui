import React from 'react';
// import cn from 'classnames';

export default function DialogFooter({ children }) {

  return (
    <div
    // className={cn(s.footer)}
    >
      {children}
    </div>
  );
};

DialogFooter.propTypes = {
};

DialogFooter.defaultProps = {
};