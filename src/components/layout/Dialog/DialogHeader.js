import React from 'react';
import { string } from 'prop-types';
// import cn from 'classnames';

export default function DialogHeader({ children, leftButton, rightButton }) {

  return (
    <div
    // className={cn(s.header)}
    >
      {leftButton &&
        <div
        // className={cn(s.left)}
        >
          {leftButton}</div>}
      {rightButton &&
        <div
        // className={cn(s.right)}
        >
          {rightButton}</div>}
      {children}
    </div>
  );
};

DialogHeader.propTypes = {
  backgroundColor: string
};

DialogHeader.defaultProps = {
};