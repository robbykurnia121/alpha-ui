import React from 'react';
import Dialog from './Dialog';
import DialogBody from './DialogBody';
import DialogFooter from './DialogFooter';
import DialogHeader from './DialogHeader';

export default {
  title: 'Container/Dialog',
  component: Dialog,
  argTypes: {
  },
};


const Template = (args) => (
  <Dialog {...args}>
    {/* button X untuk header selalu dikanan */}
    <DialogHeader >
      <div>Dialog Header children</div>
    </DialogHeader>

    <DialogBody>
      <div>Dialog Body children</div>
    </DialogBody>

    <DialogFooter>
      <div>Dialog Footer children</div>
    </DialogFooter>
  </Dialog>
);

export const Default = Template.bind({});
Default.args = {
};
