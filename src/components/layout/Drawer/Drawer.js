import React from 'react';
import PropTypes, { any, bool, func, oneOf } from 'prop-types';
import Modal from '../../basic/Modal';

export default function Drawer({ show, onClose, children, from, half }) {

  return (
    <Modal onClose={onClose} show={show} from={from} variant='drawer' maxWidth={half ? '80%' : '100%'}>
      {children}
    </Modal>
  );
};

Drawer.propTypes = {
  show: bool.isRequired,
  children: any,
  onClose: func,
  from: oneOf(['bottom', 'left', 'right']),
  half: bool
};

Drawer.defaultProps = {
  onClose: null,
  children: null,
  from: 'bottom',
  half: false
};