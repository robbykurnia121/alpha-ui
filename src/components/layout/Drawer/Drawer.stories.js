import React, { useState } from 'react';
import Drawer from './Drawer';

export default {
  title: 'Layout/Drawer',
  component: Drawer,
  argTypes: {
  },
};

const Template = (args) => {
  const [show, setShow] = useState(false);
  const [show2, setShow2] = useState(false);
  const [show3, setShow3] = useState(false);

  return (
    <div>

      <div onClick={() => setShow(!show)}>Bottom Drawer</div>
      <Drawer {...args} show={show} onClose={() => setShow(!show)}>
        <div>Test Test</div>
      </Drawer>

      <div onClick={() => setShow2(!show2)}>Half Left Drawer</div>
      <Drawer {...args} show={show2} onClose={() => setShow2(!show2)} from='left' half>
        <div>Test Test</div>
      </Drawer>

      <div onClick={() => setShow3(!show3)}>RIght Drawer</div>
      <Drawer {...args} show={show3} onClose={() => setShow3(!show3)} from='right'>
        <div>Test Test</div>
      </Drawer>
    </div>
  );
};

export const Default = Template.bind({});
Default.args = {
};