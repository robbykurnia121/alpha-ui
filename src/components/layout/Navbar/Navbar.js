import React from 'react';
import { any, string } from 'prop-types';
// import s from './Navbar.module.css';
import { Image } from '../../basic';

export default function Navbar({
  backgroundColor,
  logo,
  primaryNav,
  secondaryNav,
  searchbar,
}) {

  return (
    <div
      // className={s.root} 
      style={{ backgroundColor }}>
      {logo &&
        <Image image={logo} />
      }
      {primaryNav}
      {searchbar}
      {secondaryNav}
    </div>
  );
};

Navbar.propTypes = {
  backgroundColor: string,
  logo: string,
  primaryNav: any,
  secondaryNav: any,
  searchbar: any,
};

Navbar.defaultProps = {
  backgroundColor: 'white',
  logo: '',
  primaryNav: null,
  secondaryNav: null,
  searchbar: null,
  mobileNav: null,
  isMobile: false
};