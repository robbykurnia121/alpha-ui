import React from 'react';
import Navbar from './Navbar';
import {
  Nav, 
  NavItem,
} from '../../basic';

export default {
  title: 'Layout/Navbar',
  component: Navbar,
  argTypes: {
  },
};

const PrimaryNav = () => (
  <Nav>
    <NavItem label="Home" />
    <NavItem label="Products" />
    <NavItem label="Solutions" />
    <NavItem label="About" />
  </Nav>
);

const SecondaryNav = () => (
  <Nav>
    <NavItem label="Notifications" />
    <NavItem label="Chat" />
    <NavItem label="Accounts" />    
  </Nav>
);

const Template = (args) => (
  <Navbar {...args}
    primaryNav={<PrimaryNav />}
    secondaryNav={<SecondaryNav />}
    >
  </Navbar>
);

export const Default = Template.bind({});
Default.args = {
};