import React from 'react';
import Video from './Video';

export default {
  title: 'Basic/Video',
  component: Video,
  argTypes: {
  },
};

const Template = (args) => <Video {...args} />;

export const Default = Template.bind({});
Default.args = {
  src: 'https://www.youtube.com/embed/tgbNymZ7vqY',
  title: 'video 1'
};