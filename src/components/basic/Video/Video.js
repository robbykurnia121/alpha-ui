import React from 'react';
import { string } from "prop-types";

export default function Video({ title, src, className }) {
  return (
    <div className={className} style={{
      position: 'relative',
      paddingBottom: '56.25%'
    }}>
      <iframe
        className={'rounded-large'}
        style={{
          position: 'absolute',
          top: '0',
          left: '0',
          width: '100%',
          height: '100%',
        }}
        title={title}
        width='100%' height='100%'
        frameBorder="0"
        allowFullScreen="allowfullscreen"
        mozallowfullscreen="mozallowfullscreen"
        msallowfullscreen="msallowfullscreen"
        oallowfullscreen="oallowfullscreen"
        webkitallowfullscreen="webkitallowfullscreen"
        src={src}>
      </iframe>
    </div>
  );
};

Video.propTypes = {
  title: string,
  src: string,
  className: string
};

Video.defaultProps = {
  title: '',
  src: '',
  className: ''
};