import React from 'react';
import { SearchIcon } from '../../../icons';
import Icon from './Icon';
import { IconKindOptions } from './icon-kind';

export default {
  title: 'Basic/Icon',
  component: Icon,
  argTypes: {
    kind: {
      control: 'select',
      options: IconKindOptions, // It can't read computed values from Icon propTypes
    },
  },
};

const Template = (args) => <Icon {...args} />;

export const Default = Template.bind({});
Default.args = {
  kind: 'send',
  size: 'medium',
};

export const CustomIcon = Template.bind({});
CustomIcon.args = {
  size: 'medium',
  icon: SearchIcon
};