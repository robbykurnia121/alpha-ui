import React from 'react';
import { any, oneOf, shape } from 'prop-types';
import { IconKind, IconKindOptions } from './icon-kind';
import cn from 'classnames';

export default function Icon({ size, color, icon, kind, style }) {
  const renderIcon = (icon || (kind && IconKind[kind]));

  return renderIcon && (
    <div className={cn()} style={style}>
      {React.isValidElement(renderIcon) ? renderIcon : React.createElement(renderIcon)}
    </div>
  );
};

Icon.propTypes = {
  size: oneOf(['small', 'medium', 'large', 'xlarge']),
  color: oneOf(['default', 'primary', 'secondary', 'light', 'disabled', 'inactive']),
  kind: oneOf(IconKindOptions),
  icon: any,
  style: shape({})
};

Icon.defaultProps = {
  size: 'medium',
  color: 'primary',
  kind: null,
  icon: null,
  style: {}
};