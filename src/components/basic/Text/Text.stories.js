import React from 'react';
import Text from './Text';

export default {
  title: 'Basic/Text',
  component: Text,
  argTypes: {
  },
};

// test inherit color
// const Template = (args) => <div className='text-primary'> lala<Text {...args}>Text</Text></div>;

const Template = (args) => <Text {...args}>Text</Text>;

export const Default = Template.bind({});
Default.args = {
  className: ''
};