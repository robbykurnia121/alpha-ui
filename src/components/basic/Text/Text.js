import React, { useEffect, useRef } from 'react';
import { any, bool, func, number, oneOf, string } from 'prop-types';
import cn from 'classnames';
import { shape } from 'prop-types';

export default function Text({ size, style, component, color, children, maxLine, className, rows, gutterBottom, onClick }) {
  const textRef = useRef();
  useEffect(() => {
    if (rows > 0) {
      const lineHeight = Number(window.getComputedStyle(textRef.current, null).getPropertyValue('line-height').slice(0, -2));
      const paddingTop = Number(window.getComputedStyle(textRef.current, null).getPropertyValue('padding-top').slice(0, -2));
      const paddingBottom = Number(window.getComputedStyle(textRef.current, null).getPropertyValue('padding-bottom').slice(0, -2));
      textRef.current.style.minHeight = `${paddingTop + paddingBottom + (lineHeight * rows)}px`;
      textRef.current.style.maxHeight = `${paddingTop + paddingBottom + (lineHeight * rows)}px`;
    }
  }, [size, rows]);
  return React.createElement(
    component,
    {
      className: cn(
        `text-${size}`,
        `text-${color}`,
        `line-clamp-${maxLine || rows}`,
        gutterBottom && 'pb-2',
        onClick && 'pointer',
        className,
      ),
      onClick,
      style,
      ref: textRef
    },
    children
  );
};

Text.propTypes = {
  size: string,
  onClick: func,
  maxLine: number,
  rows: number,
  component: string, // element h1, span, etc
  className: string,
  children: any,
  color: string,
  style: shape({}),
  gutterBottom: bool,
};

Text.defaultProps = {
  size: 'md',
  onClick: null,
  maxLine: 0,
  rows: 0,
  component: 'p',
  className: '',
  children: '',
  color: 'current',
  style: {},
  gutterBottom: false,
};