import React from 'react';
import PropTypes from 'prop-types';
import {
  Text,
  Input,
  RadioGroup,
  Radio,
  CheckboxGroup,
  Checkbox,
  Select,
  Nav,
  NavItem,
} from '../..';

export default function FormField({
  type,
  size,
  label,
  options,
  inline,
  labelProps = {},
  inputProps = {},
}) {
  let inputComponent = Input;
  let defaultProps = {};

  if (type === "checkbox") {
    inputComponent = CheckboxGroup;
    defaultProps['children'] = options?.map(opt => (
      <Checkbox value={opt.value} label={opt.label} />
    ));
  }
  else if (type === "radio") {
    inputComponent = RadioGroup;
    defaultProps['children'] = options?.map(opt => (
      <Radio value={opt.value} label={opt.label} />
    ));
  }
  else if (type === "radiopill") {
    inputComponent = Nav;
    defaultProps = {
      variant: 'pill',
      titleProps: {
        color: 'light',
        colorInactive: 'primary',
        style: { fontWeight: '600' }
      },
      boxProps: {
        color: 'primary',
        colorInactive: 'primary-2',
      },
      children: options?.map(opt => (
        <NavItem title={opt.label} />
      ))
    };
  }
  else if (type === "select") {
    inputComponent = Select;
    defaultProps['options'] = options;
  }

  return (
    <div>
      {label &&
        <Text variant="body1"
          gutterBottom
          className="font-bold"
          {...labelProps}
        >{label}</Text>
      }
      {React.createElement(inputComponent, {
        size,
        type,
        inline,
        onChange: () => null,
        ...defaultProps,
        ...inputProps
      })
      }
    </div>
  );
};

FormField.propTypes = {
  variant: PropTypes.oneOf(['contained', 'outlined', 'clean']),
  size: PropTypes.oneOf(['small', 'medium', 'large']),
  type: PropTypes.oneOf(['text', 'textarea', 'number', 'email', 'date', 'time', 'datetime', 'select', 'checkbox', 'radio', 'radiopill']),
  label: PropTypes.string,
  placeholder: PropTypes.string,
  readOnly: PropTypes.bool,
  value: PropTypes.any,
  onChange: PropTypes.func,
  onReturn: PropTypes.func,
};

FormField.defaultProps = {
  variant: "contained",
  size: "medium",
  type: "text",
  readOnly: false,
};