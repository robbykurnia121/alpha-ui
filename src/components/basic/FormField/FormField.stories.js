import React from 'react';
import FormField from './FormField';

export default {
  title: 'Basic/FormField',
  component: FormField,
  argTypes: {
  },
};

const Template = (args) => <FormField {...args} />;

export const TextField = Template.bind({});
TextField.args = {
  type: "text",
  label: "Enter your name",
};

export const TextAreaField = Template.bind({});
TextAreaField.args = {
  type: "textarea",
  label: "Description",
  inputProps: {
    rows: 5,
  }
};

export const RadioField = Template.bind({});
RadioField.args = {
  type: "radio",
  label: "Gender",
  options: [
    { value: "m", label: "Male" },
    { value: "f", label: "Female" },
  ],
  inline: true,
};

export const RadioPillField = Template.bind({});
RadioPillField.args = {
  type: "radiopill",
  label: "Select consultation day",
  options: [
    { value: "1", label: "Monday" },
    { value: "2", label: "Tuesday" },
    { value: "3", label: "Wednesday" },
    { value: "4", label: "Thursday" },
    { value: "5", label: "Friday" },
  ],
};

export const CheckboxField = Template.bind({});
CheckboxField.args = {
  type: "checkbox",
  label: "Where do you learn about us?",
  options: [
    { value: "email", label: "Email" },
    { value: "web", label: "Web" },
    { value: "fb", label: "Facebook" },
    { value: "other", label: "Other" },
  ],
  inline: false,
};

export const SelectField = Template.bind({});
SelectField.args = {
  type: "select",
  label: "Select preferred payment method",
  options: [
    { value: "1", label: "Bank Transfer" },
    { value: "2", label: "Credit Card" },
    { value: "3", label: "Virtual Account" },
    { value: "4", label: "E-wallet" },
  ],
  inputProps: {
    getOptionLabel: (opt) => opt.label,
  }
};