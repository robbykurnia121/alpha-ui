import React, { Children } from 'react';
import { bool, oneOf } from 'prop-types';
import { shape } from 'prop-types';
import cn from 'classnames';
// import s from './Nav.module.css';

export default function Nav({ children, variant, align, itemSize, fullWidth, boxProps, titleProps, iconProps, subtitleProps }) {

  const childrenWithProps = children && Children.toArray(children).map((child, index) => {
    if (React.isValidElement(child)) {
      return React.cloneElement(child, {
        key: index,
        fullWidth,
        iconProps,
        boxProps,
        titleProps,
        subtitleProps,
        variant,
        align,
        itemSize,
      });
    }
    return child;
  });

  return (
    <div
    // className={cn(
    //   s.root,
    //   variant === 'tab' && s.removeSpacing,
    //   `flex ${fullWidth ? s.fullWidth : 'overflow-x-auto'}`)}
    >
      {childrenWithProps}
    </div>);
};

Nav.propTypes = {
  variant: oneOf(['tab', 'pill', 'icon']),
  align: oneOf(['left', 'center', 'right']),
  itemSize: oneOf(['small', 'medium', 'large']),
  iconProps: shape({}),
  boxProps: shape({}),
  titleProps: shape({}),
  subtitleProps: shape({}),
  fullWidth: bool
};

Nav.defaultProps = {
  variant: 'tab'
};