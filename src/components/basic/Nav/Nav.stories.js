import React from 'react';
import Nav from './Nav';
import NavItem from './NavItem';
import { SearchIcon, AddIcon, CancelIcon, CommentIcon } from '../../../icons';
import { HomeIcon, HistoryIcon, AccountIcon, ChatIcon, InboxIcon } from '../../../icons';

export default {
  title: 'Basic/Nav',
  component: Nav,
  argTypes: {
  },
};

// example Link component
const Link = ({ children, href, ...props }) => {
  return (
    <a {...props} href={href}>
      {children}
    </a>
  );
};

const Template = (args) => (
  <Nav {...args}>
    <NavItem
      title="Beranda"
      subtitle="Jelajahi beranda"
      icon={HomeIcon}
      href='/'
      Link={Link} isActive={true} />
    <NavItem
      title="Inbox"
      subtitle="Kotak masuk"
      icon={InboxIcon}
      href='/' />
    <NavItem
      title="Riwayat"
      subtitle="Lihat riwayat transaksi"
      icon={HistoryIcon}
      href='/' />
    <NavItem
      title="Pusat Bantuan"
      subtitle="Dapatkan bantuan"
      icon={ChatIcon}
      Link={Link}
      href='/' />
    <NavItem
      title="Akun"
      subtitle="Liat dan atur akun"
      icon={AccountIcon}
      Link={Link}
      href='/' />      
  </Nav>
);

export const Tab = Template.bind({});
Tab.args = {
  variant: 'tab',
  titleProps: {
    style: { fontWeight: '600' }
  },
  boxProps: {
    color: 'default',
    padding: 'medium',
    variant: 'contained'
  }
};

export const MainMenu = Template.bind({});
MainMenu.args = {
  variant: 'icon',
  itemSize: 'xlarge',
  iconProps: {
    size: 'large'
  },
  titleProps: {
    style: { fontWeight: '600' },
    show: true
  },
  subtitleProps: {
    show: true
  }
};

export const LargeIcon = Template.bind({});
LargeIcon.args = {
  variant: 'icon',
  itemSize: 'xlarge',
  iconProps: {
    size: 'xlarge'
  },
  titleProps: {
    style: { fontWeight: '600' },
    show: true
  },
  align: 'center'
};

export const BottomNav = Template.bind({})
BottomNav.args = {
  variant: 'icon',
  fullWidth: true,
  boxProps: {
    variant: 'cleaned',
    color: 'default'
  },
  titleProps: {
    variant: 'body2',
    color: 'primary',
    colorInactive: 'disabled',
    show: true
  },
  iconProps: {
    color: 'primary',
    colorInactive: 'disabled',
    style: { fontSize: '2rem' }
  }  
};

export const Pill = Template.bind({});
Pill.args = {
  variant: 'pill',
  titleProps: {
    color: 'light',
    colorInactive: 'primary',
    style: { fontWeight: '600' }
  },
  boxProps: {
    color: 'primary',
    colorInactive: 'primary-2',
  }
};

export const SmallIcon = Template.bind({});
SmallIcon.args = {
  variant: 'icon',
  fullWidth: false,
  boxProps: {
    variant: 'cleaned',
    color: 'default'
  },
  titleProps: {
    variant: 'body2'
  }
};
