import React from 'react';
import { func, oneOf, string, shape, any, bool } from 'prop-types';
import Icon from '../Icon/Icon';
import Text from '../Text/Text';
import Box from '../Box';

export default function NavItem({
  title, titleProps, subtitle, subtitleProps, icon, iconProps, boxProps,
  image, src, iconSize, variant, className, isActive, align, fullWidth, onClick, Link, href, navItemRef }) {

  const titleComponent = () => <div><Text {...titleProps} color={isActive ? titleProps?.color || 'default' : titleProps?.colorInactive}>{title}</Text></div>;
  const subtitleComponent = () => <div><Text {...subtitleProps} color={isActive ? subtitleProps?.color || 'default' : subtitleProps?.colorInactive}>{subtitle}</Text></div>;
  const imageComponent = () => <>{image({ src })}</>;
  const iconComponent = () => <div><Icon icon={icon} {...iconProps} color={isActive ? iconProps?.color || 'default' : iconProps?.colorInactive}></Icon></div>;

  const renderPill = () => {
    return (
      <div className={`w-max ${fullWidth ? 'w-full' : ''}`}>
        <Box variant='contained' {...boxProps} color={isActive ? boxProps?.color || 'transparent' : boxProps?.colorInactive}>
          {titleComponent()}
        </Box>
      </div>
    );
  };

  const renderIcon = () => {
    return (
      <div className={`${fullWidth ? 'w-full' : ''}`}>
        <Box variant='outlined' {...boxProps}>
          <div
            className={`flex flex-col justify-center 
            ${align === 'center' ? 'items-center text-center' : ''} 
            ${align === 'right' ? 'items-end text-right' : ''}`}
          >
            {image && imageComponent()}
            {icon && (
              <div>
                {iconComponent()}
              </div>
            )}
            {titleProps?.show && titleComponent()}
            {subtitleProps?.show && subtitleComponent()}
          </div>
        </Box>
      </div>
    );
  };

  const renderTab = () => {
    return (
      <div className={`w-max ${fullWidth ? 'w-full' : ''}`}>
        <Box variant='cleaned' {...boxProps}>
          <div className={`h-2`}></div>
          {titleComponent()}
          <div className={`h-1 w-6 rounded mx-auto mt-1 ${isActive ? 'bg-primary' : 'bg-default'}`}></div>
        </Box>
      </div>
    );
  };

  const renderTab2 = () => {
    return (
      <div className={`w-max ${fullWidth ? 'w-full' : ''}`}>
        <Box variant='cleaned' {...boxProps}>
          <div className={`h-2`}></div>
          {titleComponent()}
          <div className={`h-px w-full rounded mx-auto mt-1 ${isActive ? 'bg-primary' : 'bg-default'}`}></div>
        </Box>
      </div>
    );
  };

  const renderNavItem = () => (
    <div ref={navItemRef} onClick={onClick}>
      {variant === 'tab' && renderTab()}
      {variant === 'tab2' && renderTab2()}
      {variant === 'pill' && renderPill()}
      {variant === 'icon' && renderIcon()}
    </div>
  );

  return (
    <>
      {Link && href ? (
        Link({ children: renderNavItem(), href })
      ) : (
        renderNavItem()
      )}
    </>
  );

};

NavItem.propTypes = {
  navItemRef: any,
  title: any,
  subtitle: any,
  titleProps: shape({}),
  subtitleProps: shape({}),
  boxProps: shape({}),
  iconProps: shape({}),
  variant: oneOf(['tab', 'pill', 'icon']),
  align: oneOf(['left', 'center', 'right']),
  onClick: func,
  Link: func,
  href: string,
  isActive: bool
};

NavItem.defaultProps = {
  navItemRef: null,
  title: null,
  subtitle: null,
  titleProps: {},
  subtitleProps: {},
  boxProps: {},
  iconProps: {},
  variant: 'tab',
  align: 'center',
  onClick: null,
  Link: null,
  href: '',
  isActive: false
};