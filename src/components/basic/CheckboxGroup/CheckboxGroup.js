import React from 'react';
import { func } from 'prop-types';
import cn from 'classnames';

export default function CheckboxGroup({ children, onChange, values, inline }) {

  const onCheckboxChange = ((value, checked) => {
    let newValues = [...(values || [])];
    if (checked) {
      newValues.push(value);
    } else {
      const index = newValues.indexOf(value);
      newValues.splice(index, 1);
    }
    onChange(newValues);
  });

  const childrenWithProps = children.map((child, index) => {
    if (React.isValidElement(child)) {
      return React.cloneElement(child, {
        onChange: (check) => onCheckboxChange(child.props.value, check),
        checked: values?.includes(child.props.value),
        key: index,
      });
    }
    return child;
  });

  return (
    <div className={cn('flex', inline ? 'flex flex-row flex-wrap space-x-4' : 'flex flex-col space-y-1')}>
      {childrenWithProps}
    </div>
  );
};

CheckboxGroup.propTypes = {
  onChange: func,
};

CheckboxGroup.defaultProps = {
  onChange: null
};