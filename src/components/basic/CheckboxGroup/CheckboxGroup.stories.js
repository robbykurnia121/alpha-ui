import React, { useState } from 'react';
import CheckboxGroup from './CheckboxGroup';
import Checkbox from '../Checkbox/Checkbox';

export default {
  title: 'Basic/CheckboxGroup',
  component: CheckboxGroup,
  argTypes: {
  },
};

const Template = (args) => {
  const [values, setValues] = useState([0, '']);

  return (
    <CheckboxGroup {...args} values={values} onChange={setValues}>
      <Checkbox label='Option 1' value={0} />
      <Checkbox label='Option 2' value={'value 2'} />
      <Checkbox label='Option 3' value={''} />
      <Checkbox label='Option 4' value={'value 3'} />
      <Checkbox label='Option 5' value={'value 5'} />
    </CheckboxGroup >);
};

export const Default = Template.bind({});
Default.args = {
};