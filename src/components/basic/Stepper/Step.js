import React from 'react';
import { any, bool, func } from 'prop-types';
import cn from 'classnames';
import Icon from '../Icon';

export default function Step({ children, active, icon, onClick }) {

  console.log('active', active);

  return (
    <div>
      <div
        onClick={onClick}
      // className={cn(s.step, onClick && 'pointer')}
      >
        {icon && <Icon icon={icon} />}
        <div
          className={cn(
            // s.label,
            // active && s.active
          )}>
          {children}
        </div>
      </div>
    </div>
  );
};

Step.propTypes = {
  onClick: func,
  active: bool,
  icon: any
};

Step.defaultProps = {
  onClick: null,
  active: false,
  icon: null
};