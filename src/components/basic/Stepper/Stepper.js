import React, { Children, Fragment } from 'react';
import { any, bool, number } from 'prop-types';
import cn from 'classnames';

export default function Stepper({ children, separator, activeStep }) {

  const childrenWithProps = children && Children.toArray(children).map((child, index) => {
    if (React.isValidElement(child)) {
      return (
        <Fragment key={index} >
          {index > 0 && <div
          //  className={cn(s.separator)}
          >{separator}</div>}
          {React.cloneElement(child, {
            active: index === activeStep
          })}
        </Fragment>
      );
    }
    return child;
  });

  return (
    <div className={cn('flex items-center flex-wrap')}>
      {childrenWithProps}
    </div>
  );
};

Stepper.propTypes = {
  separator: any,
  flexWrap: bool,
  activeStep: number,
};

Stepper.defaultProps = {
  separator: '>',
  activeStep: 0,
};