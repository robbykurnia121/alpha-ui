import React from 'react';
import Stepper from './Stepper';
import Step from './Step';
import Icon from '../Icon/Icon';

export default {
  title: 'Basic/Stepper',
  component: Stepper,
  argTypes: {
  },
};

const Template = (args) => {
  return (
    <Stepper {...args} >
      <Step icon={<Icon kind={'add'} />}>Beranda</Step>
      <Step onClick={() => null}>Produk</Step>
      <Step>Handphone</Step>
      <Step>Handphone</Step>
      <Step>Handphone</Step>
    </Stepper >
  );
};

export const Default = Template.bind({});
Default.args = {
};