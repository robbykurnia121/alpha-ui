import React from 'react';
import Tag from './Tag';

export default {
  title: 'Basic/Tag',
  component: Tag,
  argTypes: {
  },
};

const Template = (args) => <Tag {...args} />;

export const Default = Template.bind({});
Default.args = {
};