import React from 'react';
import PropTypes from 'prop-types';

export default function Tag({ }) {

  return (
    <div>
      Not Implemented
    </div>
  );
};

Tag.propTypes = {
  checked: PropTypes.bool.isRequired,
  onChange: PropTypes.func,
  label: PropTypes.string,
};

Tag.defaultProps = {
  checked: false,
};