
import React, { useEffect, useRef, useState } from 'react';
import { any, bool, func, oneOf, string } from 'prop-types';
import Portal from '@reach/portal';
// import cn from 'classnames';

export default function Modal({
  show, closeButton, ref, children, parentRef, widthParent, containerEl, onClose: onCloseProps,
  maxHeight, maxWidth, minimumBottom, variant, from, popoverSize, disableOverlay
}) {
  const containerChildrenRef = useRef(null);
  const [showChildren, setShowChildren] = useState(false);
  const [showModal, setShowModal] = useState(false);
  const [initPosition, setInitPosition] = useState({});

  useEffect(() => {

    if (show) {
      setShowModal(show);
      setTimeout(() => {
        document.body.style.overflow = "hidden";
        setShowChildren(show);
        if (parentRef?.current && containerChildrenRef?.current) {
          const { left, top, height } = parentRef.current.getBoundingClientRect();

          const childrenHeight = containerChildrenRef.current.offsetHeight;
          containerChildrenRef.current.style.top = top + height + 'px';
          containerChildrenRef.current.style.left = left + 'px';

          if (top + childrenHeight > window.innerHeight) {
            containerChildrenRef.current.style.top = 'unset';
            containerChildrenRef.current.style.bottom = minimumBottom;
          }

          if (widthParent) {
            const width = parentRef.current.offsetWidth;
            containerChildrenRef.current.style.width = width + 'px';
          }
          containerChildrenRef.current.style.transformOrigin = `center top`;
        }
      }, variant !== 'fullscreen' ? 200 : 0);
    } else if (!show) {
      document.body.style.overflow = "";
      setShowChildren(show);
      setTimeout(() => {
        setShowModal(show);
      }, variant !== 'fullscreen' ? 250 : 0);
    }

  }, [parentRef, containerEl, show, variant, widthParent, minimumBottom, from]);

  useEffect(() => {

  }, [show]);

  const onClickOverlay = (e) => {
    e.stopPropagation();
    // showChildren & showModal for prevent breaking transition
    if (onCloseProps && showChildren & showModal) onCloseProps();
  };

  const onClickContent = (e) => {
    e.stopPropagation();
  };

  const onClickClose = (e) => {
    e.stopPropagation();
    if (onCloseProps) onCloseProps();
  };

  const onTouchStart = (e) => {
    // console.log('onTouchStart e.targetTouches[0].pageX', e.targetTouches[0].pageX);
    const x = e.targetTouches[0].pageX;
    const y = e.targetTouches[0].pageY;
    console.log('containerChildrenRef.current.offsetTop', containerChildrenRef.current.offsetTop);
    console.log('y', y);
    setInitPosition({ x, y });

  };
  const onTouchMove = (e) => {
    // console.log('onTouchMove e.targetTouches[0].pageX', e.targetTouches[0].pageX);
    // console.log('onTouchMove e.targetTouches[0].pageY', e.targetTouches[0].pageY);
    // console.log('e', e);
    const y = e.targetTouches[0].pageY;
    const { x: initX, y: initY } = initPosition;
    const diffY = y - initY;
    console.log('=================');
    console.log('initY', initY);
    console.log('y', y);
    console.log('diffY + y', diffY + y);
    console.log('y - initY', y - initY);
    console.log('y + initY', y + initY);
    console.log('=================');
    if (y >= 0) {
      containerChildrenRef.current.style.top = `${y}px`;
    }
  };
  const onTouchEnd = (e) => {
    console.log('onTouchEnd e.changedTouches[0].pageY', e.changedTouches[0].pageY);
    const y = e.changedTouches[0].pageY;
    const { y: initY } = initPosition;
    const diffY = initY - y;
    if (y < 48) {
      containerChildrenRef.current.style.borderRadius = `0px`;
      setInitPosition({ x: initPosition.x, y: y });
    } else {
      containerChildrenRef.current.style.borderRadius = `20px 20px 0px 0px`;
      setInitPosition({ x: initPosition.x, y: y });
    }
  };

  return (
    <Portal>
      {showModal && (
        <div
          // className={cn(
          //   s.overlay,
          //   showChildren && s.showOverlay,
          //   disableOverlay && s.disableOverlay,
          //   s[`${variant}Overlay`]
          // )}
          onClick={onClickOverlay}
        >
          <div
            ref={containerChildrenRef}
            onClick={onClickContent}
            // onTouchMove={onTouchMove}
            // onTouchStart={onTouchStart}
            // onTouchEnd={onTouchEnd}
            // className={cn(
            //   showChildren && s.showChildren,
            //   s.childrenContainer,
            //   s[`from--${from}`],
            //   s[`modalVariant--${variant}`],
            //   s[`popoverSize--${popoverSize}`],
            // )}
            style={{ maxHeight, maxWidth }}
          >
            {variant === 'drawer' && from === 'bottom' && (
              <div className='bg-accents-5 mx-auto h-1 w-8 rounded-full'></div>
            )}
            {children}
          </div>
        </div>
      )}
    </Portal>
  );
};

Modal.propTypes = {
  show: bool.isRequired,
  onClose: func,
  children: any,
  variant: oneOf(['menu', 'popover', 'fullscreen', 'drawer', 'bottomDrawer']),
  popoverSize: oneOf(['small', 'medium', 'large', 'fullscreen']),
  closeButton: bool,
  disableOverlay: bool,
  parentRef: any,
  widthParent: bool,
  maxWidth: string,
  maxHeight: string,
  minimumBottom: string,
  from: oneOf(['left', 'right', 'top', 'bottom', 'none'])
};

Modal.defaultProps = {
  onClose: null,
  children: null,
  variant: 'fullscreen',
  popoverSize: 'small',
  closeButton: false,
  disableOverlay: false,
  parentRef: null,
  widthParent: false,
  maxWidth: '100%',
  maxHeight: '100%',
  minimumBottom: '48px',
  from: 'none'
};