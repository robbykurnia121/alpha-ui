import React from 'react';
import ButtonGroup from './ButtonGroup';

export default {
  title: 'Basic/ButtonGroup',
  component: ButtonGroup,
  argTypes: {
  },
};

const Template = (args) => <ButtonGroup {...args} />;

export const Default = Template.bind({});
Default.args = {
};