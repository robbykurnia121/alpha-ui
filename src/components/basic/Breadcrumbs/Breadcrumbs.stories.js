import React from 'react';
import Breadcrumbs from './Breadcrumbs';
import Text from '../Text';

export default {
  title: 'Basic/Breadcrumbs',
  component: Breadcrumbs,
  argTypes: {
  },
};

const Template = (args) => {
  return (
    <Breadcrumbs {...args} >
      <Text>Beranda</Text>
      <Text>Produk</Text>
      <Text>Handphone</Text>
      <Text>Handphone</Text>
      <Text>Handphone</Text>
    </Breadcrumbs>
  );
};

export const Default = Template.bind({});
Default.args = {
};