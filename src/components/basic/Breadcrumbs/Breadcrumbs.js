import React, { Children, Fragment } from 'react';
import { any, bool } from 'prop-types';
import cn from 'classnames';

export default function Breadcrumbs({ children, separator, flexWrap }) {

  return (
    <div className={cn('flex items-center', flexWrap && 'flex-wrap')}>
      {children && Children.toArray(children).map((child, index) => (
        <Fragment key={index}>
          {index > 0 && <div className={cn('mx-2xs')}>{separator}</div>}
          <div className={index !== Children.toArray(children).length - 1 ? 'line-clamp-1' : ''}>{child}</div>
        </Fragment>
      ))}
    </div>
  );
};

Breadcrumbs.propTypes = {
  separator: any,
  flexWrap: bool,
};

Breadcrumbs.defaultProps = {
  separator: '>',
  flexWrap: false,
};