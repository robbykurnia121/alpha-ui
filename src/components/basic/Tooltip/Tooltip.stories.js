import React from 'react';
import Tooltip from './Tooltip';

export default {
  title: 'Basic/Tooltip',
  component: Tooltip,
  argTypes: {
  },
};

const Template = (args) => <Tooltip {...args} />;

export const Default = Template.bind({});
Default.args = {
};