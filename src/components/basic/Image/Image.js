import React from 'react';
import PropTypes from 'prop-types';

export default function Image({ src, radius = '0px', aspectRatio, alt = '', image }) {

  if (image) {
    return image;
  }

  if (aspectRatio !== 0) {
    return (
      <div style={{
        width: '100%',
        height: 'auto',
        position: 'relative',
        paddingBottom: `${100 / aspectRatio}%`
      }}>
        <div style={{
          borderRadius: radius,
          width: '100%',
          height: '100%',
          position: 'absolute',
          top: 0,
          left: 0,
          overflow: 'hidden',
          // This is most reliable solution to fill image in the box
          backgroundImage: `url(${src})`,
          backgroundSize: 'cover',
          backgroundRepeat: 'no-repeat',
          backgroundPosition: 'center'
        }}>
        </div>
      </div>
    );
  }

  return <img src={src} alt={alt} />;
};

Image.propTypes = {
  src: PropTypes.string.isRequired,
  aspectRatio: PropTypes.number,
};

Image.defaultProps = {
  aspectRatio: 0,
};