import React from 'react';
import Image from './Image';

export default {
  title: 'Basic/Image',
  component: Image,
  argTypes: {
  },
};

const Template = (args) => <Image {...args} />;

export const Default = Template.bind({});
Default.args = {
  src: 'https://media.goapp.co.id/62124827269192/image_thumb/catalog/category/70755182048840.jpg',
  aspectRatio: 1
};