import React, { useEffect, useRef } from 'react';
import PropTypes, { any, oneOf, string } from 'prop-types';
import cn from 'classnames';
import autosize from 'autosize';
import moment from 'moment';
import DayPickerInput from 'react-day-picker/DayPickerInput';
// import { ClearIcon, SearchIcon } from '../../../icons';
import 'react-day-picker/lib/style.css';

export default function Input({
  // styling
  roundedSize,
  variant,
  color,
  focusColor,

  placeholder, readOnly, value, type, formatString,
  onChange, onClear, onClick, rowsMax, rows, inputRef, IconSearch,
  rounded, onReturn, showClearButton, showSearchButton


}) {

  const taRef = useRef();
  const dateRef = useRef();
  const inputEl = ['text', 'number', 'email', 'time', 'datetime', 'date'];
  const timeType = ['time', 'datetime', 'date'];

  useEffect(() => {
    if (type === 'textarea') {
      const paddingTop = Number(window.getComputedStyle(taRef.current, null).getPropertyValue('padding-top').slice(0, -2));
      const paddingBottom = Number(window.getComputedStyle(taRef.current, null).getPropertyValue('padding-bottom').slice(0, -2));
      const lineHeight = Number(window.getComputedStyle(taRef.current, null).getPropertyValue('line-height').slice(0, -2));
      taRef.current.style.minHeight = `${paddingTop + paddingBottom + (lineHeight * rows)}px`;
      taRef.current.style.maxHeight = `${paddingTop + paddingBottom + (lineHeight * rowsMax)}px`;
      autosize(taRef.current);
    }
  }, [rows, rowsMax, type]);


  useEffect(() => {
    if (type === 'date') {
      console.log('dateRef', dateRef);
      console.log('dateRef.current.input.className', dateRef.current.input.className);
      dateRef.current.input.className = variantStyle[variant];
    }

  }, [type, variant]);

  const onChangeInput = (e) => {
    if (timeType.includes(type) && formatString) {
      const formatValue = moment(e.currentTarget.value).format(formatString);
      onChange(formatValue);
    }
    onChange(e.currentTarget.value);
  };

  const onSubmit = (e) => {
    e.preventDefault();
    if (onReturn) onReturn();
  };

  const onDayClick = (e) => {
    const date = moment(e).format(formatString);
    onChange(date);
  };

  const variantStyle = {
    simple: `block w-full rounded-${roundedSize} border border-${color} shadow-s focus:border-${focusColor} focus:ring focus:ring-${focusColor} focus:ring-opacity-50`,
    underline: `block w-full border-0 border-b-2 border-${color} focus:ring-0 focus:border-${focusColor}`,
    solid: `block w-full rounded-${roundedSize} bg-${color} border border-transparent focus:border-${focusColor} focus:bg-white focus:ring-0`
  };

  return (
    <div ref={inputRef}>
      {type === 'textarea' && (
        <textarea
          ref={taRef}
          type='text'
          className={cn(
            variantStyle[variant],
            // s.inputSize,
          )}
          value={value}
          placeholder={placeholder}
          readOnly={readOnly}
          onChange={(e) => onChangeInput(e)}
        />
      )}

      {(type !== 'textarea' && type !== 'date') && (
        <input
          type={type}
          className={cn(
            variantStyle[variant],
            // s.inputSize,
          )}
          value={value}
          placeholder={placeholder}
          readOnly={readOnly}
          onChange={(e) => onChangeInput(e)}
        />
      )}

      {type === 'date' && (
        <div className='relative'>
          <DayPickerInput
            ref={dateRef}
            dayPickerProps={{
              placeholder: 'YYYY-MM-DD'
            }}
            value={value}
            placeholder='YYYY-MM-DD'
            onDayChange={onDayClick}
          />
          <div className={cn('absolute inset-y-0 right-0 flex items-center pr-2 pointer-events-none')}>
            <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none">
              <path d="M19 3H18V2C18 1.45 17.55 1 17 1C16.45 1 16 1.45 16 2V3H8V2C8 1.45 7.55 1 7 1C6.45 1 6 1.45 6 2V3H5C3.89 3 3.01 3.9 3.01 5L3 19C3 20.1 3.89 21 5 21H19C20.1 21 21 20.1 21 19V5C21 3.9 20.1 3 19 3ZM18 19H6C5.45 19 5 18.55 5 18V8H19V18C19 18.55 18.55 19 18 19ZM8 10H11C11.55 10 12 10.45 12 11V14C12 14.55 11.55 15 11 15H8C7.45 15 7 14.55 7 14V11C7 10.45 7.45 10 8 10Z" fill="#CACCCF" />
            </svg>
          </div>
        </div>
      )}

      <style jsx="true">
        {`

        .DayPickerInput{
          width: 100%;
        }
        .DayPickerInput > input {
          outline: none;
          padding-top: 0.5rem;
          padding-right: 0.75rem;
          padding-bottom: 0.5rem;
          padding-left: 0.75rem;
          font-size: 1rem;
          line-height: 1.5rem;
        }
        `}
      </style>
    </div>
  );
};

Input.propTypes = {
  roundedSize: string,
  color: string,
  focusColor: string,
  type: oneOf(['text', 'textarea', 'number', 'tel', 'search', 'password', 'email', 'date', 'month', 'week', 'time', 'datetime-local']),
  variant: oneOf(['simple', 'underline', 'solid']),



  inputRef: any,
  placeholder: PropTypes.string,
  readOnly: PropTypes.bool,
  rows: PropTypes.number,
  rowsMax: PropTypes.number,
  value: PropTypes.any,
  onChange: PropTypes.func,
  onClick: PropTypes.func,
  onReturn: PropTypes.func,
  formatString: PropTypes.string,
  showClearButton: PropTypes.bool,
  showSearchButton: PropTypes.bool,
  onClear: PropTypes.func,
};

Input.defaultProps = {
  roundedSize: 'md',
  type: "text",
  variant: "simple",
  color: 'grey',
  focusColor: 'green',




  inputRef: null,
  placeholder: "Cari disini",
  formatString: 'YYYY-MM-DD',
  rowsMax: 5,
  rows: 2,
  size: "medium",
  readOnly: false,
  showClearButton: false,
  showSearchButton: false,
};