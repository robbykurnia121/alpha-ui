import React from 'react';
import { any, func } from 'prop-types';
// import cn from 'classnames';


export default function MenuItem({ children, onClick }) {

  return (
    <div
      // className={cn(s.menuItem, textS.body1)}
      onClick={onClick}>
      {children}
    </div>
  );
};

MenuItem.propTypes = {
  children: any,
  onClick: func
};

MenuItem.defaultProps = {
  children: null,
  onClick: null
};