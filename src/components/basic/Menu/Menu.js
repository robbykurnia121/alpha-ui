import React from 'react';
import { any, bool, func, string } from 'prop-types';
import Modal from '../Modal';
// import cn from 'classnames';


export default function Menu({ children, show, onClose, parentRef, maxWidth, maxHeight, widthParent, ...props }) {
  return (
    <Modal
      variant={'menu'}
      show={show}
      onClose={onClose}
      parentRef={parentRef}
      maxWidth={maxWidth}
      maxHeight={maxHeight}
      widthParent={widthParent}
      {...props}
    >
      <div
      // className={cn(s.menuContainer)}
      >
        <div
        // className={cn(s.menu)}
        >
          {children}
        </div>
      </div>

    </Modal>
  );
};

Menu.propTypes = {
  children: any.isRequired,
  show: bool.isRequired,
  onClose: func.isRequired,
  parentRef: any.isRequired,
  maxWidth: string,
  maxHeight: string,
  widthParent: bool
};

Menu.defaultProps = {
  maxWidth: '100%',
  maxHeight: 'calc(100% - 96px)',
  widthParent: false
};