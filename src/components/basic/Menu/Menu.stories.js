import React, { useEffect, useRef, useState } from 'react';
import Text from '../Text';
import Menu from './Menu';
import MenuItem from './MenuItem';

export default {
  title: 'Basic/Menu',
  component: Menu,
  argTypes: {
  },
};

const Template = (args) => {
  const [show, setShow] = useState(false);
  const [show2, setShow2] = useState(false);
  const buttonRef = useRef(null);
  const buttonRef2 = useRef(null);

  useEffect(() => {
    if (args.show) {
      setShow(args.show);
    } else if (args.show === false) {
      setShow(args.show);
    }
  }, [args.show]);

  return (
    <div>
      <div>Lorem</div>
      <div>Lorem</div>
      <div>Lorem</div>
      <div>Lorem</div>
      <div>Lorem</div>
      <div>Lorem</div>
      <div>Lorem</div>
      <div>Lorem</div>
      <div>Lorem</div>
      <div>Lorem</div>
      <div>Lorem</div>
      <div>Lorem</div>
      <div>Lorem</div>
      <div>Lorem</div>
      <div>Lorem</div>
      <div>Lorem</div>
      <div>Lorem</div>
      <div>Lorem</div>
      <div>Lorem</div>
      <div className='flex'>
        <div>Lalalala</div>
        <div className='bg-red fixed top-0 left-0' ref={buttonRef} onClick={() => setShow(true)}>Open Menussssssss</div>
        <div className='bg-red' ref={buttonRef2} onClick={() => setShow2(true)}>Open Menussssssss</div>
      </div>
      <Menu parentRef={buttonRef2} show={show2} onClose={() => setShow2(!show2)} widthParent disableOverlay>
        <MenuItem>Lorem</MenuItem>
        <MenuItem>Lorem lalalalalla lalalalla</MenuItem>
        <MenuItem><Text rows={1}>Lorem long text text text text texttext</Text></MenuItem>
      </Menu>
      <Menu parentRef={buttonRef} show={show} onClose={() => setShow(!show)} widthParent>
        <MenuItem>Lorem</MenuItem>
        <MenuItem>Lorem lalalalalla lalalalla</MenuItem>
        <MenuItem><Text rows={1}>Lorem long text text text text texttext</Text></MenuItem>
        <MenuItem>Lorem</MenuItem>
        <MenuItem>Lorem</MenuItem>
        <MenuItem>Lorem</MenuItem>
        <MenuItem>Lorem</MenuItem>
        <MenuItem>Lorem</MenuItem>
        <MenuItem>Lorem</MenuItem>
        <MenuItem>Lorem</MenuItem>
        <MenuItem>Lorem</MenuItem>
        <MenuItem>Lorem</MenuItem>
        <MenuItem>Lorem</MenuItem>
        <MenuItem>Lorem</MenuItem>
        <MenuItem>Lorem</MenuItem>
        <MenuItem>Lorem</MenuItem>
        <MenuItem>Lorem</MenuItem>
        <MenuItem>Lorem</MenuItem>
        <MenuItem>Lorem</MenuItem>
        <MenuItem>Lorem</MenuItem>
        <MenuItem>Lorem</MenuItem>
        <MenuItem>Lorem</MenuItem>
        <MenuItem>Lorem</MenuItem>
        <MenuItem>Lorem</MenuItem>
        <MenuItem>Lorem</MenuItem>
        <MenuItem>Lorem</MenuItem>
        <MenuItem>Lorem 2</MenuItem>
        <MenuItem>Lorem</MenuItem>
        <MenuItem>Lorem</MenuItem>
      </Menu>
      <div>Lorem</div>
      <div>Lorem</div>
      <div>Lorem</div>
      <div>Lorem</div>
      <div>Lorem</div>
      <div>Lorem</div>
      <div>Lorem</div>
      <div>Lorem</div>
      <div>Lorem</div>
      <div>Lorem</div>
      <div>Lorem</div>
      <div>Lorem</div>
      <div>Lorem</div>
      <div>Lorem</div>
      <div>Lorem</div>
      <div>Lorem</div>
      <div>Lorem</div>
      <div>Lorem</div>
      <div>Lorem</div>
      <div>Lorem</div>
      <div>Lorem</div>
      <div>Lorem</div>
      <div>Lorem</div>
      <div>Lorem</div>
      <div>Lorem</div>
    </div>
  );
};

export const Default = Template.bind({});
Default.args = {
};