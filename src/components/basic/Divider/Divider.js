import React, { useRef } from 'react';
import cn from 'classnames';
import { bool } from 'prop-types';

export default function Divider() {
  const hrRef = useRef();

  return (
    <hr ref={hrRef} className={cn('border border-grey')} />
  );
};

Divider.propTypes = {
  removePadding: bool
};

Divider.defaultProps = {
};