import React from 'react';
import Divider from './Divider';

export default {
  title: 'Basic/Divider',
  component: Divider,
  argTypes: {
  },
};

const Template = (args) => <Divider {...args} />;

export const Default = Template.bind({});
Default.args = {
};