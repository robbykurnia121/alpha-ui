import React from 'react';
import Badge from './Badge';

export default {
  title: 'Basic/Badge',
  component: Badge,
  argTypes: {
  },
};

const Template = (args) => <Badge {...args} />;

export const Default = Template.bind({});
Default.args = {
};