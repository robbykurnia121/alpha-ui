import React from 'react';
import { string } from 'prop-types';
import cn from 'classnames';

export default function Badge({ color, borderColor, size }) {

  return (
    <div className={cn(`bg-${color} border-${borderColor} badge-size-${size} border-2 rounded-full`)} />
  );
};

Badge.propTypes = {
  color: string,
  borderColor: string,
  size: string,
};

Badge.defaultProps = {
  color: 'green',
  borderColor: 'white',
  size: 'md',
};