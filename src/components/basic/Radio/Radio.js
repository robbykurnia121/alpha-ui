import React from 'react';
import PropTypes from 'prop-types';

export default function Radio({ checked, label, onChange }) {
  return (
    <label className="flex items-center">
      <input
        className='form-radio text-primary border border-primary'
        onChange={(e) => onChange(e.target.checked)}
        checked={checked}
        type="radio"
      />
      < span className="ml-2.5 text-base font-semibold text-accents-7">{label}</span>
    </label>
  );
};

Radio.propTypes = {
  checked: PropTypes.bool.isRequired,
  onChange: PropTypes.func,
  label: PropTypes.any,
};

Radio.defaultProps = {
  checked: false,
  label: null
};