import React from 'react';
import Radio from './Radio';

export default {
  title: 'Basic/Radio',
  component: Radio,
  argTypes: {
  },
};

const Template = (args) => <Radio {...args} />;

export const Default = Template.bind({});
Default.args = {
};