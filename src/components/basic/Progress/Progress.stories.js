import React from 'react';
import Progress from './Progress';

export default {
  title: 'Basic/Progress',
  component: Progress,
  argTypes: {
  },
};

const Template = (args) => <Progress {...args} />;

export const Default = Template.bind({});
Default.args = {
};