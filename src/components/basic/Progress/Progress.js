import React from 'react';
import PropTypes from 'prop-types';


export default function Progress({ value, color, variant }) {

  return (
    <div className='h-10 w-10'>
      {variant === 'circular' ? (
        <>
          <div className='absolute w-full'>
            <svg viewBox="0 0 100 100" xmlns="http://www.w3.org/2000/svg">
              <circle cx="50" cy="50" r="45">
              </circle>
              {value && (
                <text x="50%" y="50%" textAnchor="middle" alignmentBaseline="middle"
                  stroke={color === 'primary' ? '#fd8624' : ''}
                  strokeWidth="2px" dy="0em">
                  {value}
                </text>
              )}

            </svg>
          </div>

          <style jsx="true">
            {`
            svg {
              animation: ${value ? '' : '2s linear infinite svg-animation'};
              max-width: 100px;
            }
  
            @keyframes svg-animation {
              0% {
                transform: rotateZ(0deg);
              }
              100% {
                transform: rotateZ(360deg)
              }
            }
  
            circle {
              animation: ${value ? '' : '1.4s ease-in-out infinite both circle-animation'};
              display: block;
              fill: transparent;
              stroke: var(--${color});
              stroke-linecap: round;
              stroke-dasharray: 283;
              stroke-dashoffset: ${value ? 283 - (value * 2.83) : 280};
              stroke-width: 10px;
              transform: rotateZ(-90deg);
              transform-origin: 50% 50%;
            }

            @keyframes circle-animation {
              0 %,
              25 % {
                stroke-dashoffset: 280;
              transform: rotate(0);
              }

            50%,
            75% {
              stroke-dashoffset: 75;
              transform: rotate(45deg);
              }

            100% {
              stroke-dashoffset: 280;
              transform: rotate(360deg);
              }
            }`}
          </style>
        </>
      )
        : (
          <>
            <progress className="pure-material-progress-linear" />
            <style jsx="true">
              {`
              .pure-material-progress-linear {
                -webkit-appearance: none;
                -moz-appearance: none;
                appearance: none;
                border: none;
                height: 0.25em;
                color: var(--primary);
                background-color: #fd86;
                font-size: 16px;
              }

              .pure-material-progress-linear::-webkit-progress-bar {
                  background-color: transparent;
              }

              /* Determinate */
              .pure-material-progress-linear::-webkit-progress-value {
                  background-color: currentColor;
                  transition: all 0.2s;
              }

              .pure-material-progress-linear::-moz-progress-bar {
                  background-color: currentColor;
                  transition: all 0.2s;
              }

              .pure-material-progress-linear::-ms-fill {
                  border: none;
                  background-color: currentColor;
                  transition: all 0.2s;
              }

              .pure-material-progress-linear:indeterminate {
                  background-size: 200% 100%;
                  background-image: linear-gradient(to right, transparent 50%, currentColor 50%, currentColor 60%, transparent 60%, transparent 71.5%, currentColor 71.5%, currentColor 84%, transparent 84%);
                  animation: pure-material-progress-linear 1.5s infinite linear;
              }

              .pure-material-progress-linear:indeterminate::-moz-progress-bar {
                  background-color: transparent;
              }

              .pure-material-progress-linear:indeterminate::-ms-fill {
                  animation-name: none;
              }

              @keyframes pure-material-progress-linear {
                  0% {
                      background-size: 200% 100%;
                      background-position: left -31.25% top 0%;
                  }
                  50% {
                      background-size: 800% 100%;
                      background-position: left -49% top 0%;
                  }
                  100% {
                      background-size: 400% 100%;
                      background-position: left -102% top 0%;
                  }
              }

              `}
            </style>
          </>
        )}
    </div>
  );
};

Progress.propTypes = {
  variant: PropTypes.oneOf(['linear', 'circular']),
  color: PropTypes.oneOf(['default', 'inherit', 'primary', 'secondary']),
  size: PropTypes.oneOf(['small', 'medium', 'large']),
  label: PropTypes.string,
  value: PropTypes.number,
};

Progress.defaultProps = {
  color: 'primary',
  value: null
};