import React from 'react';
import Switch from './Switch';

export default {
  title: 'Basic/Switch',
  component: Switch,
  argTypes: {
  },
};

const Template = (args) => <Switch {...args} />;

export const Default = Template.bind({});
Default.args = {
};