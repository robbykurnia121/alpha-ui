import React from 'react';
import PropTypes from 'prop-types';

export default function Switch({ }) {

  return (
    <div>
      Not Implemented
    </div>
  );
};

Switch.propTypes = {
  checked: PropTypes.bool.isRequired,
  onChange: PropTypes.func,
  label: PropTypes.string,
};

Switch.defaultProps = {
  checked: false,
};