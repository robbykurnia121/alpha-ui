import React, { useState } from 'react';
import RadioGroup from './RadioGroup';
import Radio from '../Radio/Radio';

export default {
  title: 'Basic/RadioGroup',
  component: RadioGroup,
  argTypes: {
  },
};

const Template = (args) => {
  const [values, setValues] = useState('');
  return (
    <RadioGroup {...args} values={values} onChange={setValues}>
      <Radio label='Option 1' value={'value 1'} />
      <Radio label='Option 2' value={'value 2'} />
      <Radio label='Option 3' value={'value 3'} />
      <Radio label='Option 4' value={'value 4'} />
      <Radio label='Option 5' value={'value 5'} />
    </RadioGroup>);
};

export const Default = Template.bind({});
Default.args = {
};