import React, { useEffect } from 'react';
import { func } from 'prop-types';
import cn from 'classnames';

export default function RadioGroup({ children, onChange, values, inline }) {

  const onChangeRadio = (value) => {
    onChange(value);
  };

  useEffect(() => {
    children.map((child, index) => {
      if (React.isValidElement(child) && child?.props?.checked && child?.props?.value) {
        if (onChange) onChange(child?.props?.value);
      }
      return null;
    });
  }, [children, onChange]);

  const childrenWithProps = children.map((child, index) => {
    if (React.isValidElement(child)) {
      return React.cloneElement(child,
        {
          checked: values === child.props.value,
          onChange: () => onChangeRadio(child.props.value),
          key: index
        });
    }
    return child;
  });

  return (
    <div className={cn('flex', inline ? 'flex flex-row space-x-4' : 'flex flex-col space-y-1')}>
      {childrenWithProps}
    </div>
  );
};

RadioGroup.propTypes = {
  onChange: func
};

RadioGroup.defaultProps = {
  onChange: null
};