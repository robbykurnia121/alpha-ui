import React from 'react';
import PropTypes from 'prop-types';

export default function Skeleton({ }) {

  return (
    <div>
      Not Implemented
    </div>
  );
};

Skeleton.propTypes = {
  variant: PropTypes.oneOf(['text', 'rect', 'circle']),
  width: PropTypes.number,
  height: PropTypes.number,
};

Skeleton.defaultProps = {
  variant: 'text'
};