import React from 'react';
import Skeleton from './Skeleton';

export default {
  title: 'Basic/Skeleton',
  component: Skeleton,
  argTypes: {
  },
};

const Template = (args) => <Skeleton {...args} />;

export const Default = Template.bind({});
Default.args = {
};