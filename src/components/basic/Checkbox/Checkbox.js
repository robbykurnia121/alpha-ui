import React from 'react';
import { bool, func, string } from 'prop-types';
import cn from 'classnames';

export default function Checkbox({ checked, label, onChange, roundedSize, borderColor, size, font }) {

  return (
    <label className={cn('flex items-center space-x-2')}>
      <input
        onChange={(e) => onChange(e.target.checked)}
        checked={checked}
        type="checkbox"
        className={cn(`rounded-${roundedSize} form-checkbox text-${borderColor} ring-transparent focus:ring-transparent`)} />
      <span className={cn(`text-${size} font-${font}`)}>{label}</span>
    </label>
  );
};

Checkbox.propTypes = {
  checked: bool,
  onChange: func,
  value: string,
  label: string,
  roundedSize: string,
  borderColor: string,
  size: string,
  font: string,
};

Checkbox.defaultProps = {
  checked: false,
  onChange: null,
  value: '',
  label: '',
  roundedSize: 'sm',
  borderColor: 'grey',
  size: 'md',
  font: 'semibold',
};