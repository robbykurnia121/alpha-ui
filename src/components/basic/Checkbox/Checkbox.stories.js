import React, { useEffect, useState } from 'react';
import Checkbox from './Checkbox';

export default {
  title: 'Basic/Checkbox',
  component: Checkbox,
  argTypes: {
  },
};

const Template = (args) => {
  const [checked, setChecked] = useState(args.checked);

  useEffect(() => {
    setChecked(args.checked);
  }, [args.checked]);

  return (<Checkbox {...args} label={args.label} checked={checked} onChange={setChecked} />);
};

export const Default = Template.bind({});
Default.args = {
  checked: true,
  label: 'test'
};