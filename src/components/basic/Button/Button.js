import React from 'react';
import { any, bool, func, oneOf, string } from 'prop-types';
import cn from 'classnames';
import { IconKind, IconKindOptions } from '../Icon/icon-kind';

function Button(props) {
  const { kind } = props;
  const {
    children,
    target,
    href,
    Link,
    label = props.title, // Can use title as well
    onClick,
    icon = (kind && IconKind[kind]),
    // endIcon,
    variant,
    size,
    paddingSize,
    roundedSize,
    textColor,
    color,
    fullWidth,

    disabled,
    // loading,
  } = props;

  // const {
  //   showIcon = Boolean(icon),
  //   showLabel = Boolean(label)
  // } = props;

  const rootStyle = 'inline-flex space-x-2 items-center font-medium h-full font-semi-bold';

  const variantStyle = {
    outlined: `border border-${color} text-${color} `,
    contained: `bg-${color} text-${textColor || 'white'}`,
    text: `text-${color}`,
  };

  const renderButton = () => {
    return (
      <button onClick={onClick}
        disabled={disabled}
        className={cn(
          rootStyle,
          variantStyle[variant],
          fullWidth && 'w-full',
          `px-${paddingSize} rounded-${roundedSize}`,
          // s[`buttonPadding--${size}`],
        )}>

        <div className={cn(
          fullWidth && "ml-auto mr-auto",
          "flex items-center")}>
          {icon && (
            <div className={cn(`text-${size}`)}>
              {React.isValidElement(icon) ? icon : React.createElement(icon)}
            </div>
          )}

          {variant !== "icon" && (
            <div className={cn(`text-${size}`)}>
              {label}
            </div>
          )}
        </div>
      </button>
    );
  };

  return (
    <>
      {href && Link && Link({ children: renderButton(), href, target })}

      {href && !Link && <a href={href} target={target}>{renderButton()}</a>}

      {!href && !Link && renderButton()}
    </>
  );
};

Button.propTypes = {
  // styling
  textColor: string,
  color: string,
  variant: oneOf(['contained', 'outlined', 'text']),
  size: string,
  paddingSize: string,
  roundedSize: string,
  fullWidth: bool,

  // non styling
  label: any,
  href: string,
  Link: func,
  target: string,
  kind: oneOf(IconKindOptions),

  disabled: bool,
  onClick: func,
  loading: bool,
};

Button.defaultProps = {
  // styling
  textColor: '',
  color: '',
  variant: 'contained',
  size: "md",
  paddingSize: "md",
  roundedSize: "md",
  fullWidth: false,

  // non styling
  label: '',
  target: '',
  href: '',
  Link: null,
  kind: null,

  disabled: false,
  loading: false,

};

export default Button;