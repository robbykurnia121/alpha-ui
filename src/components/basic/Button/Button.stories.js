import React from 'react';
import Button from './Button';
import { IconKindOptions } from '../Icon/icon-kind';

export default {
  title: 'Basic/Button',
  component: Button,
  argTypes: {
    kind: {
      control: 'select',
      options: IconKindOptions, // It can't read computed values from Button propTypes
    },
  },
};

const Link = ({ children, href, target = '_self' }) => {
  return (
    <a href={href} target={target}>
      {children}
    </a>
  );
};

const Template = (args) => (
  <Button {...args}>
    childrennnn
  </Button>
);

export const Contained = Template.bind({});
Contained.args = {
  label: 'Contained Button',
  variant: 'contained',
  color: 'primary',
  // Link: Link,
  // href: 'https://www.google.com', // test for another website
  // href: '/lala', // test for router
  // target: '_blank' // test target
};

export const Outlined = Template.bind({});
Outlined.args = {
  label: 'Outlined Button',
  variant: 'outlined',
  color: 'primary'
};

export const Text = Template.bind({});
Text.args = {
  label: 'Text Button',
  variant: 'text',
  color: 'primary'
};