import React from 'react';
import Box from './Box';

export default {
  title: 'Basic/Box',
  component: Box,
  argTypes: {
  },
};

const Template = (args) => (<Box {...args}><div>Hallo</div></Box>);

export const Outlined = Template.bind({});
Outlined.args = {
  padding: 'small',
  variant: 'outlined',
  color: 'primary'
};

export const Contained = Template.bind({});
Contained.args = {
  padding: 'small',
  variant: 'contained',
  color: 'primary'
};

export const Cleaned = Template.bind({});
Cleaned.args = {
  padding: 'small',
  variant: 'cleaned',
};

export const Shadow = Template.bind({});
Shadow.args = {
  padding: 'small',
  variant: 'outlined',
  color: 'primary',
  shadowSize: 'medium'
};