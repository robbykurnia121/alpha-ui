import React from 'react';
import { bool, oneOf, shape, string } from 'prop-types';
import cn from 'classnames';

export default function Box({ paddingSize, color, textColor, variant, children, shadowSize, fullWidth, roundedSize, style }) {
  const variantStyle = {
    outlined: `border border-${color} text-${textColor} p-${paddingSize}`,
    contained: `bg-${color} text-${textColor} p-${paddingSize}`,
    clean: `text-${textColor}`,
  };

  return (
    <div className={cn(fullWidth && 'w-full')}>
      <div style={style} className={cn(
        variantStyle[variant],
        `rounded-${roundedSize} shadow-${shadowSize}`
      )}
      >
        {children}
      </div>
    </div >
  );
};

Box.propTypes = {
  roundedSize: string,
  paddingSize: string,
  color: string,
  textColor: string,
  variant: oneOf(['outlined', 'contained', 'cleaned']),
  shadowSize: string,
  fullWidth: bool,
  style: shape({}),
  // active: bool,
};

Box.defaultProps = {
  roundedSize: 'md',
  paddingSize: 'sm',
  color: 'black',
  textColor: 'black',
  variant: 'contained',
  shadowSize: 'md',
  fullWidth: true,
  style: {},
  // active: false,
};