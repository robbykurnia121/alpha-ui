import React, { Fragment } from 'react';
import { Listbox, Transition } from '@headlessui/react';
import { string } from 'prop-types';
// import cn from 'classnames';

export default function Select({
  selected, onChange, options, roundedSize
  // value, inputValue, options, onChange, onInputChange, rounded,
  // placeholder, showClearButton, onClear, getOptionLabel, variant, rightIcon
}) {

  console.log('selected component', selected);


  return (
    <div className="w-72 fixed">
      <Listbox value={selected} onChange={onChange}>
        <div className="relative">
          <Listbox.Button
            className={`rounded-${roundedSize} relative w-full py-2 pl-3 pr-10 text-left bg-white shadow-md cursor-pointer focus:outline-none focus-visible:ring-2 focus-visible:ring-opacity-75 focus-visible:ring-white focus-visible:ring-offset-orange-300 focus-visible:ring-offset-2 focus-visible:border-indigo-500`}>
            <span className="block truncate">{selected.name}</span>
            <span className="absolute inset-y-0 right-0 flex items-center pr-2 pointer-events-none">
              v
            </span>
          </Listbox.Button>
          <Transition
            as={Fragment}
            leave="transition ease-in duration-100"
            leaveFrom="opacity-100"
            leaveTo="opacity-0"
          >
            <Listbox.Options className={`rounded-${roundedSize} absolute w-full py-1 mt-1 overflow-auto text-base bg-white shadow-lg max-h-60 ring-1 ring-black ring-opacity-5 focus:outline-none`}>
              {options.map((person, personIdx) => (
                <Listbox.Option
                  key={personIdx}
                  className={({ active }) =>
                    `${active ? 'text-black bg-grey' : 'text-grey'}
                          cursor-pointer select-none relative py-2 pl-10 pr-4`
                  }
                  value={person}
                >
                  {({ active }) => {
                    // console.log('selected', selected);
                    // console.log('active', active);
                    // console.log('person.name', person.name);
                    // console.log('selected.name', selected.name);
                    return (
                      <>
                        <span
                          className={`${selected.name === person.name ? 'font-medium' : 'font-normal'
                            } block truncate`}
                        >
                          {person.name}
                        </span>
                        {selected.name === person.name ? (
                          <span
                            className={`text-primary absolute inset-y-0 left-0 flex items-center pl-3`}
                          >
                            {'>'}
                          </span>
                        ) : null}
                      </>
                    );
                  }}
                </Listbox.Option>
              ))}
            </Listbox.Options>
          </Transition>
        </div>
      </Listbox>
    </div>
  );
};

Select.propTypes = {
  roundedSize: string
  // options: PropTypes.array.isRequired,
  // rounded: oneOf(['none', 's', 'm', 'l', 'xl']),
  // variant: PropTypes.oneOf(['contained', 'outlined']),
  // size: PropTypes.oneOf(['small', 'medium', 'large']),
  // placeholder: PropTypes.string,
  // onInputChange: PropTypes.func,
  // getOptionLabel: PropTypes.func,
  // renderOption: PropTypes.func,
  // groupBy: PropTypes.func,
  // value: PropTypes.any,
  // rightIcon: PropTypes.any,
  // onChange: PropTypes.func,
  // showClearButton: PropTypes.bool,
  // onClear: PropTypes.func,
};

Select.defaultProps = {
  roundedSize: 'md'
  // rounded: 'm',
  // variant: 'outlined',
  // size: 'medium',
  // placeholder: 'Cari di sini',
  // getOptionLabel: null,
  // renderOption: null,
  // rightIcon: null,
  // groupBy: null,
  // showClearButton: false,
  // onClear: null
};
