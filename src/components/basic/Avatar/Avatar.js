import React from 'react';
import { bool, oneOf, string } from 'prop-types';
import Image from '../Image/Image';
import Badge from '../Badge/Badge';
import cn from 'classnames';

export default function Avatar({ src, text, color, size, textSize, badgePosition, active = null }) {

  const badgePositionStyle = {
    'top-right': 'top-0 right-0',
    'top-left': 'top-0 left-0',
    'bottom-right': 'bottom-0 right-0',
    'bottom-left': 'bottom-0 left-0',
  };

  const renderBadge = () => {
    return (
      <div
        className={cn(badgePositionStyle[badgePosition], 'absolute flex items-center justify-center')}
        style={{
          minHeight: '32%',
          minWidth: '32%'
        }}
      >
        <Badge size={size} />
      </div>
    );
  };

  // 'display': 'flex',
  //             'justify-content': 'center',
  //             'align-items': 'center',
  //             'border-radius': '9999px',

  return src ?
    (
      <div className={cn(`avatar-size-${size} flex justify-center items-center rounded-full relative`)}>
        <div className={cn('w-full h-full')}>
          <Image radius='100%' src={src} aspectRatio={1} />
        </div>

        {active !== null && renderBadge()}
      </div>
    ) : (
      <div className={cn(`avatar-size-${size} flex justify-center items-center rounded-full relative bg-${color}`)}>
        <div className={cn(`font-bold text-${textSize || size}`)}>
          {text?.slice(0, 2)}
        </div>

        {active !== null && renderBadge()}
      </div>
    );

};

Avatar.propTypes = {
  size: string,
  textSize: string,
  color: string,
  badgePosition: oneOf(['top-right', 'top-left', 'bottom-right', 'bottom-left']),

  text: string,
  src: string,
  active: bool
};

Avatar.defaultProps = {
  size: 'md',
  textSize: '',
  color: 'primary',
  badgePosition: 'bottom-right',

  text: '',
  src: '',
  active: null
};