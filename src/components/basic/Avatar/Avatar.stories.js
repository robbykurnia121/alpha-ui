import React from 'react';
import Avatar from './Avatar';

export default {
  title: 'Basic/Avatar',
  component: Avatar,
  argTypes: {
  },
};

const Template = (args) => <Avatar {...args} />;

export const Image = Template.bind({});
Image.args = {
  src: 'https://i.ibb.co/GnBndCB/Rectangle-874.png'
};
export const Text = Template.bind({});
Text.args = {
  text: 'RK'
};