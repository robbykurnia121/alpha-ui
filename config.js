const deepMerge = require('deepmerge');
const formsPlugin = require('@tailwindcss/forms');
const avatarPlugin = require('@alpha-ui-goapp/avatar-size');
const badgePlugin = require('@alpha-ui-goapp/badge-size');

const colors = {
  transparent: 'transparent',
  primary: '#DC2626',
  white: '#ffffff',
  black: '#000000',
  grey: '#808080',
  green: '#008000'
};

const textColor = {
  primary: '#000000',
  white: '#ffffff',
  black: '#000000',
  grey: '#808080',
  green: '#008000'
};

const fontSize = {
  'xs': ['12px', '14px'],
  'sm': ['14px', '16px'],
  'md': ['16px', '20px'],
  'lg': ['20px', '28px'],
  'xl': ['24px', '32px'],
  '2xl': ['24px', '32px'],
};

const borderRadius = {
  'none': '0',
  'sm': '0.125rem',
  'md': '.375rem',
  'lg': '0.5rem',
  'full': '9999px',
};

const padding = {
  '0': '0px',
  '1': '0.25rem',
  '2': '0.5rem',
  '3': '0.75rem',
  '4': '1rem',
  '5': '1.25rem',
  '6': '1.5rem',
  '7': '1.75rem',
  '8': '2rem',
  '9': '2.25rem',
  '10': '2.5rem',
  '2xs': '2px',
  'xs': '4px',
  'sm': '8px',
  'md': '16px',
  'lg': '24px',
  'xl': '48px',
};

const margin = {
  '0': '0px',
  '1': '0.25rem',
  '2': '0.5rem',
  '3': '0.75rem',
  '4': '1rem',
  '5': '1.25rem',
  '6': '1.5rem',
  '7': '1.75rem',
  '8': '2rem',
  '9': '2.25rem',
  '10': '2.5rem',
  '2xs': '2px',
  'xs': '4px',
  'sm': '8px',
  'md': '16px',
  'lg': '24px',
  'xl': '48px',
};

const boxShadow = {
  sm: '0 1px 2px 0 rgba(0, 0, 0, 0.05)',
  md: '0 4px 6px -1px rgba(0, 0, 0, 0.1), 0 2px 4px -1px rgba(0, 0, 0, 0.06)',
  lg: '0 10px 15px -3px rgba(0, 0, 0, 0.1), 0 4px 6px -2px rgba(0, 0, 0, 0.05)',
  xl: '0 20px 25px -5px rgba(0, 0, 0, 0.1), 0 10px 10px -5px rgba(0, 0, 0, 0.04)',
};

const backgroundOpacity = (theme) => ({
  '10': '0.1',
  ...theme('opacity'),
});

const maxHeight = (theme) => ({
  '0': '0',
  xl: '36rem',
  ...theme('spacing'),
});

const alphaUiConfig = {
  darkMode: 'class',
  // mode: 'jit',
  purge: {
    content: [
      'node_modules/alpha-ui/lib/defaultTheme.js',
      'node_modules/alpha-ui/dist/index.js',
    ],
    // options: {
    //   safelist: ['bg-primary'],
    // },
  },
  theme: {
    textColor,
    colors,
    fontSize,
    borderRadius,
    padding,
    margin,
    boxShadow,
    backgroundOpacity,
    maxHeight,
  },
  variants: {
    backgroundOpacity: ['responsive', 'hover', 'focus', 'dark'],
    backgroundColor: ['responsive', 'hover', 'focus', 'active', 'odd', 'dark'],
    display: ['responsive', 'dark'],
    textColor: ['responsive', 'focus', 'focus-within', 'hover', 'active', 'dark'],
    placeholderColor: ['responsive', 'focus', 'dark'],
    borderColor: ['responsive', 'hover', 'focus', 'dark'],
    divideColor: ['responsive', 'dark'],
    boxShadow: ['responsive', 'hover', 'focus', 'dark'],
    margin: ['responsive', 'last'],
  },
  plugins: [formsPlugin, avatarPlugin, badgePlugin],
};

function arrayMergeFn(destinationArray, sourceArray) {
  return destinationArray.concat(sourceArray).reduce((acc, cur) => {
    if (acc.includes(cur)) return acc;
    return [...acc, cur];
  }, []);
}

/**
 * Merge AlphaUi and Tailwind CSS configurations
 * @param {object} tailwindConfig - Tailwind config object
 * @return {object} new config object
 */
function wrapper(tailwindConfig) {
  let purge;
  if (Array.isArray(tailwindConfig.purge)) {
    purge = {
      content: tailwindConfig.purge,
    };
  } else {
    purge = tailwindConfig.purge;
  }
  return deepMerge({ ...tailwindConfig, purge }, alphaUiConfig, { arrayMerge: arrayMergeFn });
}

module.exports = wrapper;
